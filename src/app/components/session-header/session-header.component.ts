import { Component, Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'session-header',
  templateUrl: 'session-header.component.html',
  styleUrls: ['session-header.component.scss']
})

export class SessionHeaderComponent {
  @Input() title;
  @Input() description;

  constructor(){

  }
}
