import { Component, Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'link-see-more',
  templateUrl: 'link-see-more.component.html',
  styleUrls: ['link-see-more.component.scss']
})

export class LinkSeeMoreComponent {
  @Input() title;
  @Input() action: Function;

  constructor(){

  }  
}
