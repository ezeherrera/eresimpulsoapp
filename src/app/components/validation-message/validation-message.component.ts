import { Component, Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'validation-message',
  templateUrl: 'validation-message.component.html',
  styleUrls: ['validation-message.component.scss']
})

export class ValidationMessageComponent {
  @Input() title;
  @Input() description;
  @Input() type;
  @Input() icon;

  constructor() {
  }
}
