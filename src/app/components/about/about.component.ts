import { Component, AfterViewInit } from '@angular/core';

import { GtmService } from 'app/services/gtm.service';
import { UserService } from 'app/services/user.service';

import { ActivatedRoute } from '@angular/router';

declare var jQuery: any;

export class JuryMember {
  id: number;
  hash: string;
  name: string;
  position: string;
  description: string;
  phrase: string;
  image: string;
  logo: string;
}

@Component({
  selector: 'app-about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.scss']
})

export class AboutComponent implements AfterViewInit{
  jury: JuryMember[] = [
      {
        id: 0,
        hash: 'aurelie-salvaire',
        name: 'Aurelie Salvaire',
        position: 'Fundadora de Shiftbalance y organizadora del TEDxBarcelonaWomen 2015',
        image: './assets/images/about/members/aurelie-salvaire.png',
        logo: './assets/images/about/logo-1.png',
        // tslint:disable-next-line:max-line-length
        description: '<p>Invertir en ella cuando perdió su trabajo, hizo que se encontrara con personas claves para su futuro y su desarrollo personal. Es la fundadora de Shiftbalance, una asociación dedicada a promocionar soluciones innovadoras de equidad de género, y se dedica a formar a mujeres emprendedoras y ejecutivas en varias partes del mundo con el objetivo de invitarlas a contar su lado de la realidad y equilibrar el mundo de las conferencias. También ha organizado varios eventos en el campo de la innovación social con Ashoka e Impact HUB y es la creadora de TEDxBarcelonaWomen.</p>',
        phrase: 'Start small, but start.'
      },
      {
        id: 1,
        hash: 'alexandra-mitjans',
        name: 'Alexandra Mitjans',
        position: 'Directora Relaciones Institucionales y Directora de Ashoka Cataluña',
        image: './assets/images/about/members/alexandra-mitjans.png',
        logo: './assets/images/about/logo-2.png',
        // tslint:disable-next-line:max-line-length
        description: '<p>A Alexandra le apasiona exprimir la vida saliendo de su zona de confort y considera que soñar es obligatorio. Es licenciada en Dirección y Administración de empresas por ESADE y la Singapore Management University y ha desarrollado su carrera profesional entre el sector privado y el terciario. Actualmente es la directora de Relaciones Institucionales de Ashoka en España y Directora de Ashoka en Cataluña, donde lidera la estrategia para crear una red de colaboradores empresariales y particulares que sean protagonistas del cambio.</p>',
        phrase: 'El dolor y el miedo son las auténticas oportunidades de crecimiento que nos presenta la vida.'
      },
      {
        id: 2,
        hash: 'clara-navarro',
        name: 'Clara Navarro',
        position: 'Cofundadora de Ship2B',
        image: './assets/images/about/members/clara-navarro.png',
        logo: './assets/images/about/logo-3.png',
        // tslint:disable-next-line:max-line-length
        description: '<p>El día en que dijo “sí, quiero” a Ship2B, su vida cambió para siempre. Es licenciada y MBA por ESADE, tiene un Máster en Gestión Internacional (MIM) por la CEMS y en Medio Ambiente y Desarrollo por la London School of Economics. Ha trabajado como consultora en McKinsey & Company en Madrid y Londres. Y ha sido cofundadora de Anoderworld y organizadora de Startup Pirates Barcelona. También es docente e investigadora en emprendimiento social e innovación en ESADE, la Universidad de Barcelona y la Universittà Bocconi de Milán.</p>',
        phrase: 'Aceptar tus limitaciones y pedir ayuda, te hace más fuerte.'
      },
      {
        id: 3,
        hash: 'sonia-navarro',
        name: 'Sonia Navarro',
        position: 'Directora del Instituto para la Innovación Social de ESADE',
        image: './assets/images/about/members/sonia-navarro.png',
        logo: './assets/images/about/logo-4.png',
        // tslint:disable-next-line:max-line-length
        description: '<p>Es licenciada en Ingeniería de comunicaciones y electrónica por la Universidad de Northumbria (Reino Unido) y ha obtenido el FT MBA, el FGONG y el LIS en ESADE. Tras cursar el MBA, trabajó en Londres en una start up americana del sector de telecomunicaciones como responsable del Departamento de marketing europeo. En 2006, se incorporó en ESADE Alumni para lanzar el programa Alumni Solidario mediante el cual antiguos alumnos de ESADE ofrecen acompañamiento y consultoría pro bono para ONG’s. Desde enero de 2008, forma parte del equipo del Instituto de Innovación Social de ESADE, donde actualmente es la directora asociada y codirectora de los cursos de formación para ONG’s.</p>',
        phrase: 'Incluso en las situaciones más límite, se encuentra una forma de seguir adelante'
      },
      {
        id: 4,
        hash: 'pablo-sanchez',
        name: 'Pablo Sánchez',
        position: 'Co-fundador de BCorp España y de Roots4Sustainability',        
        image: './assets/images/about/members/pablo-sanchez.png',
        logo: './assets/images/about/logo-5.png',
        // tslint:disable-next-line:max-line-length
        description: '<p>La llamada de un buen amigo fue clave para encontrar su vocación. Hoy es socio co-fundador de Roots for Sustainability, Director de B Corp en España y acumula 15 años de experiencia en el ámbito de los negocios inclusivos, la responsabilidad social empresarial y la medida del impacto social. Doctorado en Organización de Empresas por la Universidad Politécnica de Cataluña, licenciado en Economía por la Universidad de Barcelona y diplomado en Gestión de Actividades Turísticas por la Universidad de Girona, es también Senior Research Fellow de ECRI (Ethics in Finance & Social Value) en la Universidad del País Vasco y miembro de la Junta Directiva de GEAccounting.</p>',
        phrase: 'La honestidad y las relaciones humanes son fundamentales para lograr buenos resultados.'
      },
    ];
  // selectedMember: JuryMember;
  selectedMember: boolean;
  displayJury: any = {
      'height': 'auto',
      'overflow': 'auto'
    };
  displayJuryMember: any= {
    'height': '0',
    'overflow': 'hidden',
    'opacity': '0',
    'padding': '0',
    'margin': '0'
  };

  constructor(
    private user: UserService,
    private gtm: GtmService,
  ) {
    this.selectedMember = false;
  };

  ngAfterViewInit() {

    /* Etiquetado */
    this.gtm.push({
      'pageName': '/. Home',
      'proyecto': '',
      'userId': (this.user.currentUser) ? this.user.currentUser.id : '',
    });

    // init jury owl carousel
    jQuery('#jury-owl').owlCarousel({
      loop:   false,
      margin: 0,
      nav:    true,
      navText: ['<span class="icon-arrow"></span>', '<span class="icon-arrow"></span>'],
      lazyLoad: true,
      autoHeight: true,
      responsive: {
        0: {
            items: 1
        },
        640: {
            items: 2
        },
        1024: {
            items: 5
        }
      }
    });

    // init jury member owl carousel
    jQuery('#jury-owl-member').owlCarousel({
      loop:   true,
      margin: 100,
      nav:    true,
      navText: ['<span class="icon-arrow"></span>', '<span class="icon-arrow"></span>'],
      lazyLoad: true,
      autoHeight: true,
      responsive:{
        0:{
            items: 1
        },
        640:{
            items: 1
        },
        1024:{
            items: 1
        }
      }
    });
  }

  showMember(member: JuryMember): void {
    jQuery('#jury-owl-member')
      .trigger('to.owl.carousel', [member.id, 1, true]);

    this.displayJury = {
      'height': '0',
      'overflow': 'hidden',
      'opacity': '0',
      'padding': '0',
      'margin': '0'
    };
    this.displayJuryMember = {
      'height': 'auto',
      'overflow': 'auto'
    };
  }

  hideMembers() {
    // jQuery('#jury-owl-member')
    //   .trigger("to.owl.carousel", [0, 1, true]);

    this.displayJury = {
      'height': 'auto',
      'overflow': 'auto'
    };
    this.displayJuryMember = {
      'height': '0',
      'overflow': 'hidden',
      'opacity': '0',
      'padding': '0',
      'margin': '0'
    };
  }

  // showMember(member: JuryMember): void {
  //   this.selectedMember = member;
  // }

  // nextMember(): void {
  //   const index = this.jury.indexOf(this.selectedMember);

  //   if ( (index + 1) === this.jury.length ) {
  //     this.selectedMember = this.jury[0];
  //   }else {
  //     this.selectedMember = this.jury[index + 1];
  //   }
  // }

  // previousMember(): void {
  //   let index = this.jury.indexOf(this.selectedMember);

  //   if ( index === 0 ) {
  //     index = this.jury.length;
  //   }
  //   this.selectedMember = this.jury[index - 1];
  // }

  // closeDetail(): void {
  //   this.selectedMember = null;
  // }
}
