import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        switch (route.url[0].path) {
            // in case of login screen
            case 'iniciar-sesion':
                // if user already exists
                if (!localStorage.getItem('currentUser')) {
                    return true;
                }
                // redirect to home
                this.router.navigate(['/']);
            break;

            case 'sube-tu-proyecto':
                if (localStorage.getItem('currentUser')) {
                    if (!localStorage.getItem('currentProject')) {
                        return true;
                    }
                    this.router.navigate(['/area-usuario/proyecto']);
                } else {
                    this.router.navigate(['/iniciar-sesion'], { queryParams: { returnUrl: state.url }});
                }
            break;

            default:
                if (localStorage.getItem('currentUser')) {
                    // logged in so return true
                    return true;
                }
                // not logged in so redirect to login page with the return url
                this.router.navigate(['/iniciar-sesion'], { queryParams: { returnUrl: state.url }});
            break;
        }
        return false;
    }
}
