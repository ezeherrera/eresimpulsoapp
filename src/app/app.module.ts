/* Modules */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageUploadModule } from 'app/modules/ng2-image-upload/src/image-upload.module';
import { Angular2SocialLoginModule } from 'angular2-social-login';
import { AppRoutingModule } from 'app/modules/app-routing.module';
/* Services */
import { ContactService } from 'app/services/contact.service';
import { UserService } from 'app/services/user.service';
import { ProvinceService } from 'app/services/province.service';
import { ProjectService } from 'app/services/project.service';
import { PagerService } from 'app/services/pager.service';
import { VotingService } from 'app/services/voting.service';
import { GtmService } from 'app/services/gtm.service';
/* Guards */
import { AuthGuard } from 'app/guard/auth.guard';
/* Components */
import { AppComponent } from 'app/app.component';
import { HeaderComponent } from 'app/components/header/header.component';
import { FooterComponent } from 'app/components/footer/footer.component';
import { ModalComponent } from 'app/components/modal/modal.component';
import { HomeComponent } from 'app/components/home/home.component';
import { LoginComponent } from 'app/components/login/login.component';
import { RegisterComponent } from 'app/components/register/register.component';
import { ContactComponent } from 'app/components/contact/contact.component';
import { AboutComponent } from 'app/components/about/about.component';
import { InspireComponent } from 'app/components/inspire/inspire.component';
import { InspireItemComponent } from 'app/components/inspire/components/item/inspire-item.component';
import { ResourcesComponent } from 'app/components/resources/resources.component';
import { InspireNavigationComponent } from 'app/components/inspire/components/navigation/inspire-navigation.component';
import { FinalistasNavigationComponent } from 'app/components/projects/components/navigation/finalistas-navigation.component';
import { LinkSeeMoreComponent } from 'app/components/link-see-more/link-see-more.component';
import { SessionHeaderComponent } from 'app/components/session-header/session-header.component';
import { ValidationMessageComponent } from 'app/components/validation-message/validation-message.component';
import { ProjectPaginationComponent } from 'app/components/projects/components/pagination/project-pagination.component';
import { ProjectItemComponent } from 'app/components/projects/components/item/project-item.component';
import { ProjectListComponent } from 'app/components/projects/list/list.component';
import { ProjectDetailsComponent } from 'app/components/projects/details/details.component';
import { ProjectFinalistasComponent } from 'app/components/projects/finalistas/finalistas.component';
import { ProjectFormComponent } from 'app/components/projects/form/project-form.component';
import { BannerComponent } from 'app/components/banner/banner.component';
import { FaqsComponent } from 'app/components/faqs/faqs.component';
import { FilterListPipe } from './pipes/filter-list.pipe';
import { OrderListPipe } from './pipes/order-list.pipe';
import { UserAreaComponent } from './components/user-area/user-area.component';
import { SocialLoginComponent } from './components/social-login/social-login.component';
import { LoginFormComponent } from './components/login-form/login-form.component';

const socialProviders = {
    'google': {
      'clientId': '880956469069-hc56s04i34mqast32bg62ng31g6vf99j.apps.googleusercontent.com'
    },
    /*'linkedin': {
      'clientId': 'LINKEDIN_CLIENT_ID'
    },*/
    'facebook': {
      'clientId': '305494773214352',
      'apiVersion': 'v2.9'
    }
  };

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    ImageUploadModule.forRoot(),
    Angular2SocialLoginModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ModalComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ContactComponent,
    AboutComponent,
    InspireComponent,
    InspireItemComponent,
    ResourcesComponent,

    InspireNavigationComponent,
    FinalistasNavigationComponent,

    LinkSeeMoreComponent,
    SessionHeaderComponent,
    ValidationMessageComponent,

    ProjectPaginationComponent,
    ProjectItemComponent,

    ProjectListComponent,
    ProjectDetailsComponent,
    ProjectFinalistasComponent,
    ProjectFormComponent,

    BannerComponent,
    FaqsComponent,
    FilterListPipe,
    OrderListPipe,
    UserAreaComponent,
    SocialLoginComponent,
    LoginFormComponent
  ],
  providers: [
    ContactService,
    UserService,
    ProvinceService,
    ProjectService,
    AuthGuard,
    PagerService,
    GtmService,
    VotingService
  ],
  bootstrap: [ AppComponent ]
})


export class AppModule {}

Angular2SocialLoginModule.loadProvidersScripts(socialProviders);
