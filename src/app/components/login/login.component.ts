import { Component, Input } from '@angular/core';


@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})

export class LoginComponent {

  notConfirmed: Boolean;

  constructor() {}

  onNotConfirmed(value: boolean) {
    console.log('Not Confirmed: ' + value);
    this.notConfirmed = value;
  };

}
