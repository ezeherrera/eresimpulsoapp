import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GtmService } from 'app/services/gtm.service';
import { UserService } from 'app/services/user.service';

declare var jQuery: any;

@Component({
  moduleId: module.id,
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})

export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  unsuscribe: Boolean = false;
  sub: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private user: UserService,
    private gtm: GtmService,
  ) {}

  ngOnInit() {
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        if (params['unsubscribe']) {
          this.unsuscribe = true;
        } else {
          this.unsuscribe = false;
        }
        if (params['logout']) {
          this.user.logout();
        }
      });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  ngAfterViewInit() {
    // init owl carousel
    jQuery('.owl-carousel').owlCarousel({
      loop:   false,
      margin: 50,
      nav:    false,
      responsive: {
        0: {
            items: 1
        },
        640: {
            items: 2
        },
        1024: {
            items: 3
        }
      }
    });

    /* Etiquetado */
    //this.gtm.transversalPush();
  }
}
