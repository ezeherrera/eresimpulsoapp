import { Component, Input, ElementRef } from '@angular/core';

declare var jQuery: any;

@Component({
  moduleId: module.id,
  selector: 'finalistas-navigation',
  templateUrl: 'finalistas-navigation.component.html',
  styleUrls: ['finalistas-navigation.component.scss']
})

export class FinalistasNavigationComponent {
  @Input() navs;
  @Input() action: Function;

  constructor(
    private el: ElementRef
  ){

  }  

  ngAfterViewInit() {
    // init local foundation
    jQuery(this.el.nativeElement.localName).foundation();
  }
}
