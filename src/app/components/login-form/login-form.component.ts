import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, NG_VALIDATORS } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from 'app/classes/user.class';
import { UserService } from 'app/services/user.service';
import { GtmService } from 'app/services/gtm.service';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {

  @Output() onNotConfirmed = new EventEmitter<boolean>();
  user: User;
  loginForm: FormGroup;
  returnUrl: String = '';
  errorTitle: String = ('');
  errorMessages: String = ('');
  validate: Boolean = (false);

  constructor(
    private userService: UserService,
    private gtm: GtmService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.buildForm();
  }


  buildForm(): void {
    this.loginForm = this.fb.group({
      email:    ['', [
                    Validators.required
                ]],
      password: ['', Validators.required]
    });
    this.loginForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
  }
  onValueChanged(data?: any) {
    this.errorMessages = '';
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.login();
    }
  }

  login(): any {
    this.userService.login(this.loginForm.value)
      .then( response => {
        if ( response.status ) {
          this.userService.get()
          .then( (userdata: User) => {
            const params = {
              'event': 'login',
              'event_category': 'login',
              'event_action': 'login completado',
              'event_label': '/inicia_sesion'
            };
            this.gtm.push(params);
            this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
            this.router.navigate([this.returnUrl]);
          } );
        } else {
          if (response.errorModal === '#modalValidationPending') {
            this.onNotConfirmed.emit(true);
            this.errorTitle = 'Aún no has confirmado tu registro';
            this.errorMessages = 'Accede al email de confirmación que te mandamos cuando te registraste y haz click en el link que contiene.<br/>¡Es un minuto!';
            const params = {
              'event': 'login',
              'event_category': 'login',
              'event_action': 'login incorrecto: Usuario pendiente de validación',
              'event_label': '/inicia_sesion'
            }; this.gtm.push(params);
          } else {
            this.errorMessages = 'El nombre de usuario o la contraseña no son correctos.<br/>Por favor, revísalos e inténtalo de nuevo.';
            const params = {
              'event': 'login',
              'event_category': 'login',
              'event_action': 'login incorrecto: El nombre de usuario o la contraseña no son correctos',
              'event_label': '/inicia_sesion'
            }; this.gtm.push(params);
          }
        }
      } );
  }

  restorePassword(): any {
    if (this.loginForm.get('email').valid) {
      const title = 'Revisa tu bandeja de entrada';
      const msg = 'Sigue las instrucciones del e-mail que acabamos de enviarte con la contraseña.';
      this.userService.restore({'email': this.loginForm.get('email').value})
        .then( res => {
          if (res.sendMail) {
            this.errorTitle = title;
            this.errorMessages = msg;
          }else{
            this.errorTitle = 'Upss';
            this.errorMessages = 'Lo sentimos, este usuario no existe. Revisa que el email sea correcto.';
          }
        } );
    } else {
      this.validate = true;
      this.loginForm.get('email').setErrors({ 'required': true });
    }
  }

}
