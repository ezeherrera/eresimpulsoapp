import { Component, AfterViewInit } from '@angular/core';
import { VotingService } from 'app/services/voting.service';

declare var jQuery: any;

@Component({
  selector: 'social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.scss']
})
export class SocialLoginComponent implements AfterViewInit {

  constructor(
      private voting: VotingService
  ) { }

  ngOnInit() {
  }

  signIn(provider) {
    this.voting.signIn(provider);
  }

  ngAfterViewInit() {
    // init local foundation
    jQuery('#social-login').foundation();
  }

}
