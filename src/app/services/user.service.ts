import { Injectable, Output, EventEmitter } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { User } from 'app/classes/user.class';
import { Project } from 'app/classes/project.class';
import { ProjectService } from 'app/services/project.service';

import { environment } from 'environments/environment';


@Injectable()
export class UserService {

  @Output() updatedUser = new EventEmitter;

  public currentUser: User;

  private env = environment;
  private headers = new Headers({'Content-Type': 'multipart/form-data'});
  private getHeaders = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(
    private http: Http,
    private projectService: ProjectService,
  ) {}

  get(data?: Object): Promise<any> {
    const url = this.env.apiHost + 'api/user';
    const options = { headers: this.getHeaders, withCredentials: true };
    if (!data) { data = {}; }
    return this.http
      .get(url, options )
      .toPromise()
      .then( response => {
        const res = response.json();
        if (typeof res.user === 'undefined') {
          localStorage.removeItem('currentUser');
          localStorage.removeItem('currentProject');
        }
        return res;
      })
      .then( response => {
        const user = response.user;
        if (typeof response.user !== 'undefined') {
          if (user.project_id) {
            this.projectService.getUser()
            .then( res => {
              user.project = res;
              localStorage.setItem( 'currentProject', user.project_id );
              this.set(user);
              return user;
            });
          } else {
            localStorage.removeItem( 'currentProject' );
            this.set(user);
          }
        }else{
          this.updatedUser.emit(this.currentUser);
        }
      })
      .catch(this.handleError);
  }

  set(user: User): void {
    console.log(user);
    this.currentUser = user;
    if (!localStorage.getItem( 'currentUser')) {
      localStorage.setItem( 'currentUser', user.email );
    };
    this.updatedUser.emit(this.currentUser);
  }

  login(data): Promise<any> {
    const loginUrl = this.env.apiHost + 'api/login';
    const options = { headers: this.headers, withCredentials: true };
    return this.http
      .post(loginUrl, JSON.stringify(data), options)
      .toPromise()
      .then( response => response.json() )
      .catch(this.handleError);
  }

  logout() {
    const loginUrl = this.env.apiHost + 'api/logout';
    const options = { headers: this.headers, withCredentials: true };
    return this.http
      .post(loginUrl, JSON.stringify({}), options)
      .toPromise()
      .then( response => {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentProject');
        delete this.currentUser;
        this.updatedUser.emit(this.currentUser);
      } )
      .catch(this.handleError);
  }

  register(data): Promise<any> {
    const registerUrl = this.env.apiHost + 'api/register';
    return this.http
      .post(registerUrl, JSON.stringify(data), {headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  validate(data): Promise<any> {
    const registerUrl = this.env.apiHost + 'api/activeRegister/' + data;
    return this.http
      .get(registerUrl, {headers: this.getHeaders})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  update(data): Promise<any> {
    const options = { headers: this.headers, withCredentials: true };
    const userUrl = this.env.apiHost + 'api/user';
    return this.http
      .post(userUrl, JSON.stringify(data), options)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  restore(data): Promise<any> {
    const options = { headers: this.headers };
    const userUrl = this.env.apiHost + 'api/rememberPassword';
    return this.http
      .post(userUrl, JSON.stringify(data), options)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  delete(): Promise<any> {
    const options = { headers: this.headers, withCredentials: true };
    const userUrl = this.env.apiHost + 'api/delUser';
    return this.http
      .post(userUrl, JSON.stringify({}), options)
      .toPromise()
      .then(res => {
        return res.json();
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
