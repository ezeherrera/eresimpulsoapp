import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { environment } from 'environments/environment';

@Injectable()
export class ContactService {

  private env = environment;
  private contactUrl = this.env.apiHost + 'api/contacto';
  private headers = new Headers({'Content-Type': 'multipart/form-data'});

  constructor(private http: Http) { }

  send(data): Promise<any> {
    const options = { headers: this.headers, withCredentials: true };
    return this.http
      .post(this.contactUrl, JSON.stringify(data), options)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
