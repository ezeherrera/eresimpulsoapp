import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
    name: 'filterList',
    pure: false
})
@Injectable()
export class FilterListPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {
        if (!items) { return []; }
        if (value) {
            return items.filter(item => item[field] === value);
        } else {
            return items;
        }
    }
}
