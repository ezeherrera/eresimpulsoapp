export class Province {
  id: number;
  name: string;
  postal_code: string;
}