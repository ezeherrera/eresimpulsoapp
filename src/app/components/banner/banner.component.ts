import {Component, Input} from '@angular/core';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-banner',
  templateUrl: 'banner.component.html',
  styleUrls: ['banner.component.scss']
})

export class BannerComponent {
  @Input()
  section: string;

  private env = environment;

  fbMetaHome = 'https://www.facebook.com/sharer/sharer.php?u=' + this.env.apiHost + 'public/assets/meta-share/meta-home.html';
  twMetaHome = 'https://twitter.com/share?url=' + this.env.apiHost + 'public/assets/meta-share/meta-home.html';
  ldnMetaHome = 'https://www.linkedin.com/shareArticle?url=' + this.env.apiHost + 'public/assets/meta-share/meta-home.html';

}
