import { Injectable, Output, EventEmitter } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { User } from 'app/classes/user.class';
import { environment } from 'environments/environment';
import 'rxjs/add/operator/toPromise';

@Injectable()

export class ProjectService {

  @Output() activeProject = new EventEmitter;

  public env: any;
  public ganadoras2015: Object[];
  public ganadoras2016: Object[];
  public finalistas2015: Object[];
  public finalistas2016: Object[];
  private headers = new Headers({'Content-Type': 'multipart/form-data'});

  constructor(private http: Http) {
    this.env = environment;

    /* 2015 */
    this.ganadoras2015 = [
      {
        project_id: "214",
        project_name: "Being Inclusive",
        project_status: "Ganador",
        project_description: "Being Inclusive es una empresa social de moda inclusiva que atiende a la imagen de manera integral. Visibilizamos la diversidad corporal y trabajamos por una moda más representativa.<br />\n<br />\n",
      "project_creation_datetime": "16/08/2015",
        project_main_image: "main_image.jpg",
        project_user: "Olga Mayoral Pulido"
      },
      {
        project_id: "124",
        project_name: "Ciudades Family Welcome by Mammaproof",
        project_status: "Ganador",
        project_description: "Red internacional de blogs con el objetivo de promover una ciudad más amable con los niños y sus familias. Con nuestra actividad apoyamos el negocio local y creamos empleo en el colectivo de mujeres profesionales y madres.","project_creation_datetime": "13/08/2015",
        project_main_image: "main_image.jpg",
        project_user: "Mavi Villatoro Ruiz "
      }
    ];

    this.finalistas2015 = [
      {
        project_id: "167",
        project_name: "e-nlaza: \"conecta, pregunta y simplifica tu vida con cáncer\"",
        project_status: "Finalista",
        project_description: "e-nlaza proporciona un entorno de apoyo a las personas afectadas por el cáncer, facilitando la creación de espacios privados de contacto on-line entre pacientes y profesionales. ", "project_creation_datetime": "14/08/2015",
        project_main_image: "main_image.jpg",
        project_user: "Fátima Castaño Ferrero"
      },
      {
        project_id: "161",
        project_name: "SIBARARÍ",
        project_status: "Finalista",
        project_description: "Proyecto social dirigido a mujeres de etnia gitana o en riesgo de exclusión, cuyo objetivo es  promover su inserción sociolaboral a través de una actividad creativa de reciclaje textil.",
        "project_creation_datetime": "14/08/2015",
        project_main_image: "main_image.jpg",
        project_user: "OLALLA LÓPEZ ÁLVAREZ"
      },
      {
        project_id: "149",
        project_name: "FUNTRIP4ALL",
        project_status: "Finalista",
        project_description: "Revolucionamos la experiencia de viajar, poniendo la Innovación al alcance de todos! Primera plataforma de ocio y turismo creativo e inclusivo. Conecta a viajeros con gente local, profesionales o amateurs, con o sin discapacidad. <br />\n<br />\n<br />\n",
        "project_creation_datetime": "14/08/2015",
        project_main_image: "main_image.jpg",
        project_user: "Jennifer Baños Gata"
      },    
      {
        project_id: "111",
        project_name: "EnCLAVE - Proyecto musical ",
        project_status: "Finalista",
        project_description: "Sacar brillo y color no requiere de mucho esfuerzo: ganas, ilusión y música. Porque hay talentos que merecen un reconocimiento y otros que necesitan una oportunidad - ¿Nos ayudáis a descubrirlos? Compás, Ritmo, ..Acción!!! ",
        "project_creation_datetime": "13/08/2015",
        project_main_image: "main_image.jpg",
        project_user: "María Molina Matas"
      },
      {
        project_id: "93",
        project_name: "Creación de un espacio para Sofía",
        project_status: "Finalista",
        project_description: "Sofía, asociación para la prevención y ayuda de Trastornos Alimentarios, es impulsada por una joven que tras pasar anorexia, decide emprender un nuevo camino que cambiaría su vida por completo, conseguir un centro para  TCA en Canarias.",
      "project_creation_datetime": "11/08/2015",
        project_main_image: "main_image.jpg",
        project_user: "Odalid  Molina Hernández"
      },
      {
        project_id: "81",
        project_name: "Nutricocina",
        project_status: "Finalista",
        project_description: "Nutricocina es un método de educación nutricional que enseña a los niños a comer de forma saludable usando como herramienta educativa la cocina.<br />\n",
        "project_creation_datetime": "10/08/2015",
        project_main_image: "main_image.jpg",
        project_user: "Nur Al Ali"
      },
      {
        project_id: "36",
        project_name: "inspira-t",
        project_status: "Finalista",
        project_description: "Nos gastamos millones en formación para jóvenes desempleados y el paro juvenil sigue en el 50%. Algo estamos haciendo mal. En inspira-t conseguimos activarlos de otra forma:<br />\nEscuchándoles y ayudándoles a encontrar su inspiración.",
        "project_creation_datetime": "27/07/2015",
        project_main_image: "main_image.jpg",
        project_user: "Eugenia Gargallo Guil"
      },
      {
        project_id: "34",
        project_name: "ACA - @Aloja @Convive @Aprende",
        project_status: "Finalista",
        project_description: "ACA es una Plataforma de alojamiento intergeneracional para:<br />\n1.Paliar la soledad de los ancianos y favorecer el envejecimiento activo con las IT.<br />\n2. Facilitar a los estudiantes un hogar para vivir.<br />\n",
        "project_creation_datetime": "22/07/2015",
        project_main_image: "main_image.jpg",
        project_user: "Raquel Moreno Garcia"
      }
    ];

    /* 2016 */
    this.ganadoras2016 = [
      {
        project_id: "550",
        project_name: "AdaLab. Creando Talento Digital",
        project_status: "Ganador",
        project_description: "AdaLab es un startup social cuyo objetivo es invertir en mujeres con talento que no han tenido la oportunidad de desarrollar todo su potencial, para que se conviertan en profesionales digitales y accedan a oportunidades laborales de calidad, creando el talento digital que necesitan las empresas.",
        project_creation_datetime: "15/06/2016",
        project_main_image: "main_image.png",
        project_user: "Inés "
      },
      {
        project_id: "563",
        project_name: "Es im-perfect",
        project_status: "Ganador",
        project_description: "Es im-perfect es la primera marca del Estado Español que elabora y comercializa productos de alta calidad a partir de frutas y verduras feas e imperfectas con la participación de colectivos en riesgo de exclusión social.  Un proyecto de alto impacto social y medioambiental. #lacomidanosetira",
        project_creation_datetime: "15/06/2016",
        project_main_image: "main_image.png",
        project_user: "Mireia Barba "
      } 
    ];

    this.finalistas2016 = [
      {
        project_id: "520",
        project_name: "Uttopy ",
        project_status: "Finalista",
        project_description: "Uttopy es una marca de moda solidaria que a través de su diseño, da visibilidad a causas sociales. Para ello cada mes colabora con una ONG, creando una edición limitada inspirada en su causa, y haciendo difusión de la labor de la entidad. <br />\n",
        project_creation_datetime: "14/06/2016",
        project_main_image: "main_image.jpg",
        project_user: "Inés Echevarría Soler "
      },
      {
        project_id: "512",
        project_name: "Aaitsme ",
        project_status: "Finalista",
        project_description: " A los 26 me quedé completamente calva por Alopecia Areata y esto cambió mi manera de ver la vida. <br />\n<br />\nMi objetivo es inspirar y motivar a utilizar la enfermedad como herramienta para el cambio.<br />\n<br />\nAA its me ofrece Pelucas customizadas, coaching terapéutico, eventos, workshops y retiros.",
        project_creation_datetime: "14/06/2016",
        project_main_image: "main_image.png",
        project_user: "Nuria Hijano Alarcón "
      },
      {
        project_id: "419",
        project_name: "Animosa",
        project_status: "Finalista",
        project_description: "Mujeres importantes de la Historia. Diseños únicos. Productos con alma. En Animosa ponemos en valor a las mujeres y a sus hazañas. Lo hacemos con diseños propios y en unos formatos que no dejan a nadie indiferente. Nuestra producción es Española y/o con certificación de estándares éticos de trabajo.",
        project_creation_datetime: "07/06/2016",
        project_main_image: "main_image.png",
        project_user: "Mónica Conde Mella "
      },
      {
        project_id: "406",
        project_name: "Withoutrees",
        project_status: "Finalista",
        project_description: "Hemos desarrollado un nuevo material utilizando los residuos generados por las industrias de la piedra natural. Se trata de un PAPEL AUTOADHESIVO que ni es papel ni contiene adhesivo, por lo que no daña las superficies y puedes utilizarlo cuantas veces quieras, y sin talar un sólo árbol!",
        project_creation_datetime: "04/06/2016",
        project_main_image: "main_image.jpg",
        project_user: "Esther Escolano Jover "
      },
      {
        project_id: "377",
        project_name: "Trip&Feel “Siente el Mar\"",
        project_status: "Finalista",
        project_description: "Trip&Feel ayuda a la sostenibilidad del sector pesquero, introduciendo innovación respetando la tradición del propio sector. Por ello, apostamos por la PescaTurismo, una actividad de ocio alternativo para turistas que generará ingresos extras a los pescadores y sus familias, y al ámbito local. ",
        project_creation_datetime: "25/05/2016",
        project_main_image: "main_image.jpg",
        project_user: "Sandra "
      },
      {
        project_id: "362",
        project_name: "Dona CAFE",
        project_status: "Finalista",
        project_description: "En dona CAFE tenemos un sueño, ayudar a mujeres desfavorecidas o en riesgo de exclusión social, mediante la formación barista -experta en la preparación y servicio de café- para poder desarrollarse, trabajar y emprender en el sector del café. ",
        project_creation_datetime: "12/05/2016",
        project_main_image: "main_image.jpg",
        project_user: "Eva Espinet "
      },
      {
        project_id: "318",
        project_name: "Fundación Quiero Trabajo",
        project_status: "Finalista",
        project_description: "Quiero Trabajo es una Fundación sin ánimo de lucro que asesora, instruye y viste a mujeres para que afronten con confianza y seguridad su  entrevista de trabajo  y consigan un empleo.<br />\n<br />\n",
        project_creation_datetime: "21/04/2016",
        project_main_image: "main_image.png",
        project_user: "Vanessa "
      },
      {
        project_id: "287",
        project_name: "ABSORVALIA",
        project_status: "Finalista",
        project_description: "Diseñar y producir absorbentes textiles, es decir, ropa de uso personal y para el hogar, para personas con problemas de incontinencia, ya sea de orina, fecal, exceso de salivación,...<br />\n",
        project_creation_datetime: "30/03/2016",
        project_main_image: "main_image.png",
        project_user: "Mireia Montolio Sopena "
      }      
    ];
  }

  getList(year: any): Promise<any> {
    const registerUrl = this.env.apiHost + 'api/projects/' + year;
    const options = { headers: this.headers, withCredentials: true };

    return this.http
      .post(registerUrl, JSON.stringify({}), options )
      .toPromise()
      .then(res => res.json().projects
        .map( function(item){
          const project_year = item.project_creation_datetime.slice(0, 4);
          item.project_year = year;
          return item;
        } )
      )
      .catch(this.handleError);
  }

  getUser(id?: Object): Promise<any> {
    const registerUrl = this.env.apiHost + 'api/getProject';
    const options = { headers: this.headers, withCredentials: true };

    return this.http
      .post(registerUrl, JSON.stringify({'id': id}), options )
      .toPromise()
      .then(response => {
        let project  = response.json().project;
        project['status'] = response.json().projectStatus;

        return project;
      })
      .catch(this.handleError);
  }

  get(id?: Object): Promise<any> {
    const registerUrl = this.env.apiHost + 'api/project/' + id;
    const options = { headers: this.headers, withCredentials: true };

    return this.http
      .post(registerUrl, JSON.stringify({'id': id}), options )
      .toPromise()
      .then(response => {
        this.activeProject.emit(response.json().project);
        return response.json().project;
      })
      .catch(this.handleError);
  }

  create(data): Promise<any> {
    const options = { headers: this.headers, withCredentials: true };
    const registerUrl = this.env.apiHost + 'api/addProject';
    return this.http
      .post(registerUrl, JSON.stringify(data), options)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  edit(data): Promise<any> {
    const options = { headers: this.headers, withCredentials: true };
    const registerUrl = this.env.apiHost + 'api/editProject';
    return this.http
      .post(registerUrl, JSON.stringify(data), options)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  upload(data): Promise<any> {
    const options = { headers: this.headers, withCredentials: true };
    const registerUrl = this.env.apiHost + 'api/uploadImage';
    return this.http
      .post(registerUrl, JSON.stringify(data), options)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  delete(): Promise<any> {
    const options = { headers: this.headers, withCredentials: true };
    const registerUrl = this.env.apiHost + 'api/delProject';
    return this.http
      .post(registerUrl, JSON.stringify({}), options)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
