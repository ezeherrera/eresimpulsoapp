import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderList'
})
export class OrderListPipe implements PipeTransform {

    transform(items: any[], field: string, value: string): any[] {
        if ( !items ) { return []; }
        let orderedList = [];
        orderedList = items.sort( function(a: any, b: any) {
                return a[field] - b[field];
              });
        if ( value === 'desc' ) {
          orderedList.reverse();
        }
        return orderedList;
    }

}
