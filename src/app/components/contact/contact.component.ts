import {Component, Input} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

import {ContactService} from 'app/services/contact.service';

@Component({
  moduleId: module.id,
  selector: 'app-contact',
  templateUrl: 'contact.component.html',
  styleUrls: ['contact.component.scss']
})

export class ContactComponent {
  @Input()
  contactForm: FormGroup;

  formErrors = {
    'name': '',
    'email': '',
    'subject': '',
    'phone': '',
    'message': ''
  };

  sent = false;
  error = false;
  errorMessages = "";

  constructor(private contactService: ContactService, private fb: FormBuilder) {
    this.buildForm();
  }

  buildForm(): void {
    this.contactForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: [],
      subject: ['', Validators.required],
      message: ['', [
        Validators.required,
        Validators.maxLength(1024)
      ]]
    });
    this.contactForm.valueChanges
      .subscribe(data => this.onValueChanged());

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged() {
    this.error = false;
    if (!this.contactForm) {
      return;
    }
    const form = this.contactForm;
    for (const field in this.formErrors) {
      if (field) {
        const control = form.get(field);
        this.formErrors[field] = '';
        if (control && control.dirty && !control.valid) {
          this.formErrors[field] = true;
        }
      }
    }
  }

  onSubmit() {
    if (this.contactForm.valid) {
      this.send();
    }
  }

  send(): void {
    this.sent = false;
    this.error = false;
    this.errorMessages = "";
    this.contactService.send(this.contactForm.value)
      .then(data => {
        if (data.status) {
          this.sent = true;
          this.contactForm.reset();
        } else {
          this.error = true;
          for (const error in data.errors) {
            if (data.errors.hasOwnProperty(error)) {
              this.formErrors[error] = true;
              this.errorMessages += data.errors[error];
            }
          }
        }
      });
  }
}
