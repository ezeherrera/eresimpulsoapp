import { Component, ElementRef, OnInit, AfterViewInit } from '@angular/core';

declare var jQuery: any;

@Component({
  moduleId: module.id,
  selector: 'app-modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['modal.component.scss']
})

export class ModalComponent implements OnInit, AfterViewInit {
  constructor(
    private el: ElementRef
  ) {
  }

  ngOnInit() {
    jQuery(document).on(
      'closed.zf.reveal', '[data-reveal]', function () {
        jQuery('#app-modal-content').html('');
      }
    );
  }

  ngAfterViewInit () {
    jQuery(this.el.nativeElement.localName).foundation();
  }
}
