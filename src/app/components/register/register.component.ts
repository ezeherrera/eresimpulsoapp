import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, NG_VALIDATORS } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from 'app/classes/user.class';
import { UserService } from 'app/services/user.service';
import { ProvinceService } from 'app/services/province.service';
import { GtmService } from 'app/services/gtm.service';

@Component({
  moduleId: module.id,
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.scss']
})
export class RegisterComponent {

  @Input()
  user: User;
  registerForm: FormGroup;
  sent: Boolean;
  confirmed: Boolean;
  errors: Boolean;
  loading: Boolean;
  errorMessages: String;

  formErrors = {
    'gender': false,
    'name': false,
    'email': false,
    'password': false,
    'confirm_password': false,
    'telephone': false,
    'postal_code': false,
    'province': false,
    'legal': false
  };
  validationMessages = {
    // tslint:disable-next-line:max-line-length
    'menNotAllowed': 'Eres Impulso es una plataforma dirigida a mujeres. Si conoces a alguna mujer con una idea para cambiar el mundo, anímala a participar.',
    'checkPassword': 'Las contraseñas no coinciden. Por favor, revísalos e inténtalo de nuevo.',
    'required': 'Todos los campos son obligatorios. Por favor, revísalos e inténtalo de nuevo.',
  };

  provinces = this.provinceService.getList();

  constructor(
    private userService: UserService,
    private provinceService: ProvinceService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private gtm: GtmService
  ) {
    this.errorMessages = '';

    if (this.route.snapshot.params['id']) {
      console.log(this.route.snapshot.params);
      this.userService.validate(this.route.snapshot.params['id'])
      .then(response => {
        if(response.modalValidationSuccess === true || response.modalValidated === true){
          this.router.navigate(['/iniciar-sesion']);
        }
      });
    }
    this.buildForm();
    this.loading = false;
    this.sent = false;
    this.confirmed = false;
    this.errors = false;
  }

  buildForm(): void {
    this.registerForm = this.fb.group({
      gender:      ['', [
                      Validators.required,
                      this.menNotAllowed
                    ]],
      name:        ['', Validators.required],
      email:       ['', [
                        Validators.required,
                        // tslint:disable-next-line:max-line-length
                        Validators.pattern('([A-Z|a-z|0-9](\.|_){0,1})+[A-Z|a-z|0-9]\@([A-Z|a-z|0-9])+((\.){0,1}[A-Z|a-z|0-9]){2}\.[a-z]{2,3}')
                    ]],
      password:    ['', [
                      Validators.required,
                      //this.checkPassword,
                    ]],
      confirm_password: ['', [
                      Validators.required,
                      this.checkPassword,
                    ]],
      telephone:       ['', Validators.required],
      postal_code: ['', Validators.required],
      province:    ['', Validators.required],
      legal:       ['', [
                      Validators.required,
                      this.mustBeTrue
                    ]],
    });

    /*this.registerForm.valueChanges
    .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now*/
  }

  onValueChanged(data?: any) {
    if (!this.registerForm) { return; }
    const form = this.registerForm;
    this.errorMessages = '';
    for (const field in this.formErrors) {
      if (field) {
        // reset errors from input
        this.formErrors[field] = null;
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          this.formErrors[field] = true;
          for (const key in control.errors) {
            if (!this.errorMessages && this.validationMessages[key]) {
              this.errorMessages = this.validationMessages[key];
            }
          }
        }
      }
    }
    if (this.errorMessages) {
      // Etiquetado
      const _params = {
        'event': 'registro',
        'event_category': 'registro',
        'event_action': 'registro incorrecto',
        'event_label': this.errorMessages.replace(/<br\/>/g, ' ').replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, '')
      }; this.gtm.push(_params);
    }
  }

  private checkPassword(control: FormControl) {
    const password = control.root.get('password');
    const confirm = control.root.get('confirm_password');
    if (!password || !confirm) { return; }
    return ( confirm.dirty && (confirm.value ===  password.value) ) ? null : {
      checkPassword: {
        valid: false
      }
    };
  }

  private menNotAllowed(control:  FormControl) {
    return (control.value === 'Mujer') ? null : {
      menNotAllowed: {
        valid: false
      }
    };
  }

  private mustBeTrue(control:  FormControl) {
    return (control.value === true) ? null : {
      mustBeTrue: {
        valid: false
      }
    };
  }

  validateForm(data?: any) {
    if (!this.registerForm) { return; }
    const form = this.registerForm;
    this.errorMessages = '';
    for (const field in this.formErrors) {
      if (field) {
        this.formErrors[field] = null;
        const control = form.get(field);
        if (control && !control.valid) {
          this.formErrors[field] = true;
          for (const key in control.errors) {
            if (!this.errorMessages && this.validationMessages[key]) {
              this.errorMessages = this.validationMessages[key];
            }
          }
        }
      }
    }
    if (this.errorMessages) {
      // Etiquetado
      const _params = {
        'event': 'registro',
        'event_category': 'registro',
        'event_action': 'registro incorrecto',
        'event_label': this.errorMessages.replace(/<br\/>/g, ' ').replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, '')
      }; this.gtm.push(_params);
    }
    this.registerForm.valueChanges
    .subscribe( res => this.onValueChanged(res));
  }

  onSubmit() {
    this.validateForm();
    if (this.registerForm.valid && !this.loading) {
      this.register();
    }
  }
  register(): void {
    const form = this.registerForm,
          params = {
                    name:        form.get('name').value,
                    password:    form.get('password').value,
                    sex:         form.get('gender').value,
                    email:       form.get('email').value,
                    postal_code: form.get('postal_code').value,
                    telephone:   form.get('telephone').value,
                    province:    Number(form.get('province').value),
                    acceptanceLegalBasis: Number(form.get('legal').value)
                  };
    this.loading = true;
    this.userService.register(params)
    .then(res => {
      if (typeof res.errors === 'object') {
        this.errors = true;
        this.errorMessages = '';
        for (const field in res.errors) {
          if (typeof this.formErrors[field] !== 'undefined') {
            this.formErrors[field] = true;
            this.errorMessages += res.errors[field] + '<br/>';
          }
        }
        const _params = {
          'event': 'registro',
          'event_category': 'registro',
          'event_action': 'registro incorrecto',
          'event_label': this.errorMessages.replace(/<br\/>/g, ' ').replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, '')
        }; this.gtm.push(_params);
      } else {
        this.sent = true;
        this.confirmed = true;
        const _params = {
          'event': 'registro',
          'event_category': 'registro',
          'event_action': 'registro completado',
          'event_label': '' //sólo en el caso que se produzca un error
        }; this.gtm.push(_params);
      }
      this.loading = false;
    });
  }
}
