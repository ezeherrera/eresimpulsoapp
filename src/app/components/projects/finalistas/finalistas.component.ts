import { Component } from '@angular/core';
import { ProjectService } from 'app/services/project.service';
import { Project } from 'app/classes/project.class';
import { Navs } from 'app/classes/navs.class';

declare var jQuery:any;

@Component({
  moduleId: module.id,
  selector: 'app-projectfinalistas',
  templateUrl: 'finalistas.component.html',
  styleUrls: ['finalistas.component.scss']
})

export class ProjectFinalistasComponent {
  public ganadoras2015: Object[];
  public ganadoras2016: Object[];
  public finalistas2015: Object[];
  public finalistas2016: Object[];
  public year: number;
  public navs: Navs[];  

  constructor(
    private projectService: ProjectService
  ){    
    this.ganadoras2015 = this.projectService.ganadoras2015;
    this.ganadoras2016 = this.projectService.ganadoras2016;
    this.finalistas2015 = this.projectService.finalistas2015;    
    this.finalistas2016 = this.projectService.finalistas2016;
    this.year = 2016;

    // navigation
    this.navs = [
      {
        id: 1,
        name: "2015",
        type: "2015",
        class: ""
      },
      {
        id: 1,
        name: "2016",
        type: "2016",
        class: "active"
      }
    ];        
  }

  setYear(year: number): void {       
    this.year = year;

    jQuery('.navigation-link a').removeClass('active');
    jQuery('.navigation-link__' + year + ' a').addClass('active');

    jQuery('.year').hide();
    jQuery('.year__' + year).show();
  }

  ngAfterViewInit () {   
    this.setYear(this.year);
  }
}
