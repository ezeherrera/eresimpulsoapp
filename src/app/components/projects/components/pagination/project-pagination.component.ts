import { Component, Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'project-pagination',
  templateUrl: 'project-pagination.component.html',
  styleUrls: ['project-pagination.component.scss']
})

export class ProjectPaginationComponent {
  @Input() pager: any;    
  @Input() action: Function;    

  constructor(){
    
  }    
}
