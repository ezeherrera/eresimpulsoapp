import { Project } from 'app/classes/project.class';

export class User {
  id: number;
  email: string;
  sex: string;
  name: string;
  password: string;
  lastname: string;
  postal_code: string;
  telephone: string;
  province: number;
  city: string[];
  status: string;
  occupation: string;
  acceptanceLegalBasis: number;
  acceptanceLegalBasisDatetime: string;
  deletionDate: string;
  attachedFiles: string[];
  creationDatetime: string;
  modificationDatetime: string;
  deletionDatetime: string;

  project_id: string;
  project: Project;
}
