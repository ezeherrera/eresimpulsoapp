import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

const list = [
  { id: 2,  name: 'Albacete', code:	2 },
  { id: 3,  name: 'Alicante', code:	3 },
  { id: 4,  name: 'Almería', code:	4 },
  { id: 1,  name: 'Álava', code:	1 },
  { id: 33, name: 'Asturias', code:	33 },
  { id: 5,  name: 'Ávila', code:	5 },
  { id: 6,  name: 'Badajoz', code:	6 },
  { id: 7,  name: 'Baleares, Islas', code:	7 },
  { id: 8,  name: 'Barcelona', code:	8 },
  { id: 48, name: 'Bizkaia', code:	49 },
  { id: 9,  name: 'Burgos', code:	9 },
  { id: 10, name: 'Cáceres', code:	10 },
  { id: 11, name: 'Cádiz', code:	11 },
  { id: 39, name: 'Cantabria', code:	39 },
  { id: 12, name: 'Castellón', code:	12 },
  { id: 51, name: 'Ceuta', code:	51 },
  { id: 13, name: 'Ciudad Real', code:	13 },
  { id: 14, name: 'Córdoba', code:	14 },
  { id: 15, name: 'Coruña', code:	15 },
  { id: 16, name: 'Cuenca', code:	16 },
  { id: 20, name: 'Gipuzkoa', code:	20 },
  { id: 17, name: 'Girona', code:	17 },
  { id: 18, name: 'Granada', code:	18 },
  { id: 19, name: 'Guadalajara', code:	19 },
  { id: 21, name: 'Huelva', code:	21 },
  { id: 22, name: 'Huesca', code:	22 },
  { id: 23, name: 'Jaén', code:	23 },
  { id: 24, name: 'León', code:	24 },
  { id: 27, name: 'Lugo', code:	27 },
  { id: 25, name: 'Lleida', code:	25 },
  { id: 28, name: 'Madrid', code:	28 },
  { id: 29, name: 'Málaga', code:	29 },
  { id: 52, name: 'Melilla', code:	52 },
  { id: 30, name: 'Murcia', code:	30 },
  { id: 31, name: 'Navarra', code:	31 },
  { id: 32, name: 'Ourense', code:	32 },
  { id: 34, name: 'Palencia', code:	34 },
  { id: 35, name: 'Palmas, Las', code:	35 },
  { id: 36, name: 'Pontevedra', code:	36 },
  { id: 26, name: 'Rioja, La', code:	26 },
  { id: 37, name: 'Salamanca', code:	37 },
  { id: 38, name: 'Santa Cruz de Tenerife', code:	38 },
  { id: 40, name: 'Segovia', code:	40 },
  { id: 41, name: 'Sevilla', code:	41 },
  { id: 42, name: 'Soria', code:	42 },
  { id: 43, name: 'Tarragona', code:	43 },
  { id: 44, name: 'Teruel', code:	44 },
  { id: 45, name: 'Toledo', code:	45 },
  { id: 46, name: 'Valencia', code:	46 },
  { id: 47, name: 'Valladolid', code:	47 },
  { id: 49, name: 'Zamora', code:	49 },
  { id: 50, name: 'Zaragoza', code:	50 }
];

@Injectable()
export class ProvinceService {

  constructor() {}

  getList(): Object[] {
    return list;
  }

  /*getProvince(id: number): Object {
    return list.map( function(item){
      if (item.id === this) {
        return item;
      }
    }, id );
  }*/
}
