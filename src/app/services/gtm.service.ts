import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';

import { User } from 'app/classes/user.class';
import { UserService } from 'app/services/user.service';
import { ProjectService } from 'app/services/project.service';

declare const dataLayer;

@Injectable()
export class GtmService {

  params = {
    event: 'virtual_page',
    pageName: '',
    proyecto: '',
    userID: ''
  };

  userSubscriber = null;
  projectSubscriber = null;

  constructor(
    private userService: UserService,
    private projectService: ProjectService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnDestroy() {
    this.userService.updatedUser.unsubscribe();
    this.projectService.activeProject.unsubscribe();
  }

  public push(params: Object) {
    //console.warn(params);
    dataLayer.push(params);
  }

  public transversalPush() {

    /** PAGE_NAME **/
    this.params.pageName = this.router.url;

    /** PROJECT_ID **/
    if (this.params.pageName.indexOf('proyectos/') > 0) {
      this.projectObserver();
    } else {
      this.params.proyecto = '';
      this.userObserver();
    }
  }

  public projectObserver( ) {
    if (!this.userSubscriber) {
      this.userSubscriber = this.projectService.activeProject.subscribe(
        (project) => {
          this.params.proyecto = project.project_name;
          if (Number(project.project_year) < 2017) {
            this.params.pageName = '/ediciones-anteriores/' + project.project_year + '/'  + project.project_name;
          } else {
            this.params.pageName = '/proyectos/' + project.project_name;
          }
          this.userObserver();
        });
    }
  }

  public userObserver( ) {
    if (localStorage.getItem( 'currentUser')) {
      if (typeof this.userService.currentUser === 'undefined') {
      if (!this.projectSubscriber) {
        this.projectSubscriber =
           this.userService.updatedUser.subscribe(
            (user) => {
              this.params.userID = (typeof user !== 'undefined') ? String(user.id) : '';
              this.push(this.params);
            }
          );
        }
      } else {
        this.params.userID = String(this.userService.currentUser.id);
        this.push(this.params);
      }
    } else {
      this.push(this.params);
    }
  }

  public resetSubscribers() {
    this.projectService.activeProject.unsubscribe();
    this.userService.updatedUser.unsubscribe();
  }

}
