import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, NG_VALIDATORS } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { environment } from 'environments/environment';

import { User } from 'app/classes/user.class';
import { UserService } from 'app/services/user.service';

import { Project } from 'app/classes/project.class';
import { ProjectService } from 'app/services/project.service';

import { ProvinceService } from 'app/services/province.service';

import { ValidationMessageComponent } from 'app/components/validation-message/validation-message.component';
import { ImageUploadComponent } from 'app/modules/ng2-image-upload/src/image-upload/image-upload.component';

const profileErrors = {
  'gender': false,
  'name': false,
  'email': false,
  'password': false,
  'confirm_password': false,
  'phone': false,
  'postal_code': false,
  'province': false,
  'legal': false
};
const profileMessages = {
  'checkPassword': 'Las contraseñas no coinciden. Por favor, revísalos e inténtalo de nuevo.',
  'required': 'Todos los campos son obligatorios. Por favor, revísalos e inténtalo de nuevo.'
};

@Component({
  selector: 'app-user-area',
  templateUrl: './user-area.component.html',
  styleUrls: ['./user-area.component.scss']
})

export class UserAreaComponent implements OnInit {
  uploadImageUrl = `${this.projectService.env.apiHost}api/uploadImage`;
  projectSent: Boolean;
  projectValidate: Boolean;
  profileSent: Boolean;
  confirmDeleteUser: Boolean;
  errors: Boolean;
  loading: Boolean;
  errorMessages: String;
  tab: String;
  isReadonly: Boolean = false;

  profileForm: FormGroup;
  projectForm: FormGroup;

  user: User = new User;
  project: any = {};


  formErrors = {
    'name': false,
    'email': false,
    'password': false,
    'telephone': false,
    'postal_code': false,
    'province': false,
    'legal': false
  };

  projectFormErrors = {
    'name': '',
    'description': '',
    'profile_image': '',
    'main_image': ''
  };

  question = 1;
  questions = [ 1 , 2 , 3 , 4 , 5 , 6 ];

  provinces = this.provinceService.getList();

  constructor(
    private userService: UserService,
    private provinceService: ProvinceService,
    private fb: FormBuilder,
    private projectService: ProjectService,
    private router: Router,
    private route: ActivatedRoute,
  ) {

    if (this.route.snapshot.params['tab']) {
      this.tab = this.route.snapshot.params['tab'];
    } else {
      this.tab = 'perfil';
    }

    this.setUser(this.userService.currentUser);

    this.projectSent = false;
    this.projectValidate = false;
    this.profileSent = false;
    this.confirmDeleteUser = false;
  }

  ngOnInit() {
    this.getUserData();
  }

  getUserData(): void {
    this.userService.updatedUser.subscribe(
      (res) => {
        this.setUser(res);
      }
    );
  }

  setUser(user) {
    if (typeof user !== 'undefined') {
      // initialize user and profileform
      this.user = user;
      this.buildProfileForm();

      if (typeof this.user.project !== 'undefined') {
        // initialize project and profileform
        this.project = this.user.project;
        this.buildProjectForm();
        this.isReadonly = ( this.project.project_status === 'Editando' || this.project.project_status === 'Invalidado' ) ? false : true;
      }else{
        this.project = {};
      }
    }else {
      // reset user var
      this.user = new User;
    }
  }

  buildProfileForm(): void {
    this.profileForm = this.fb.group({
      name:        [ this.user.name,        Validators.required],
      email:       [ this.user.email, [
                        Validators.required,
                        // tslint:disable-next-line:max-line-length
                        Validators.pattern('([A-Z|a-z|0-9](\.|_){0,1})+[A-Z|a-z|0-9]\@([A-Z|a-z|0-9])+((\.){0,1}[A-Z|a-z|0-9]){2}\.[a-z]{2,3}')
                    ]],
      /*password:    [ this.user.password, [
                      Validators.required,
                      //this.checkPassword,
                    ]],
      confirm_password: ['', [
                      Validators.required,
                      this.checkPassword,
                    ]],*/
      phone:       [ this.user.telephone,   Validators.required],
      postal_code: [ this.user.postal_code, Validators.required],
      province:    [ this.user.province,    Validators.required]
    });
  }

  buildProjectForm(): void {
    if ( this.project.id || this.project.project_id ) {
      this.projectForm = this.fb.group({
        project_id:        [ this.project.project_id ],
        name:              [ this.project.project_name,              [Validators.required, Validators.maxLength(40)]],
        description:       [ this.project.project_description_textarea,       [Validators.required, Validators.maxLength(300)]],
        profile_image:     [ this.project.project_profile_image,      Validators.required],
        main_image:        [ this.project.project_main_image,        Validators.required],
        web:               [ this.project.project_web],
        additional_image1: [ this.project.project_additional_image1],
        additional_image2: [ this.project.project_additional_image2],
        additional_image3: [ this.project.project_additional_image3],
        explanation:       [ this.project.project_explanation_textarea,       [Validators.maxLength(1024), Validators.required]],
        model_business:    [ this.project.project_model_business_textarea,    [Validators.maxLength(1024), Validators.required]],
        team_description:  [ this.project.project_team_description_textarea,  [Validators.maxLength(1024), Validators.required]],
        calendar:          [ this.project.project_calendar_textarea,          [Validators.maxLength(1024), Validators.required]],
        need_economic:     [ this.project.project_need_economic_textarea,     [Validators.maxLength(1024), Validators.required]],
        info_extra:        [ this.project.project_info_extra_textarea,        [Validators.maxLength(1024), Validators.required]]
      });
    }
  }


  /* PROFILE STUFF */

  private checkPassword(control: FormControl) {
    const password = control.root.get('password');
    const confirm = control.root.get('confirm_password');
    if (!password || !confirm) { return; }
    return ( confirm.dirty && (confirm.value ===  password.value) ) ? null : {
      checkPassword: {
        valid: false
      }
    };
  }

  onValueChanged(data?: any) {
    if (!this.profileForm) { return; }
    const form = this.profileForm;
    this.errorMessages = '';
    for (const field in this.formErrors) {
      if (field) {
        // reset errors from input
        this.formErrors[field] = null;
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          this.formErrors[field] = true;
        }
      }
    }
  }

  validateForm(data?: any) {
    if (!this.profileForm) { return; }
    const form = this.profileForm;
    this.errorMessages = '';
    for (const field in this.formErrors) {
      if (field) {
        this.formErrors[field] = null;
        const control = form.get(field);
        if (control && !control.valid) {
          this.formErrors[field] = true;
        }
      }
    }
    this.profileForm.valueChanges
    .subscribe( res => this.onValueChanged(res));
  }

  profileSubmit() {
    this.validateForm();
    if (this.profileForm.valid && !this.loading) {
      this.update();
    }
  }

  update(): void {
    const form = this.profileForm,
          params = {
                    id:         this.user.id,
                    name:        form.get('name').value,
                    password:    this.user.password,
                    sex:         this.user.sex,
                    email:       form.get('email').value,
                    postal_code: form.get('postal_code').value,
                    telephone:   form.get('phone').value,
                    province:    Number(form.get('province').value),
                    //acceptanceLegalBasis: Number(form.get('legal').value)
                  };
    this.loading = true;
    this.profileSent = false;
    this.userService.update(params)
    .then(res => {
      if (typeof res.errors !== 'undefined') {
        this.errors = true;
        this.errorMessages = '';
        for (const field in res.errors) {
          if (typeof this.formErrors[field] !== 'undefined') {
            this.formErrors[field] = true;
            this.errorMessages += res.errors[field] + '<br/>';
          }
        }
      } else {
        this.userService.get();
        this.profileSent = true;
      }
      this.loading = false;
    });
  }

  deleteUser(): any{
    this.confirmDeleteUser = true;
  };

  profileDelete(): void {
    this.userService.delete()
    .then(res => {
      if (res.status) {
        this.userService.logout()
        .then( res2 => {
          this.router.navigate(['/'], { queryParams: { unsubscribe: true } });
        });
      }
    });
  }



  /* PROJECT STUFF */

  projectDelete(): void {
    this.projectService.delete()
    .then(res => {
      if (res.status) {
        localStorage.removeItem( 'currentProject' );
        this.userService.get();
        this.router.navigate(['/']);
      }
    });
  }

  gotToQuestion(index: number) {
    if (!index) { return; }
    if (index <= this.questions.length && index > 0) {
      this.question = index;
    }
  }

  imageUploaded( event, input: any ) {
    /*const file = event.file;
    const formData = new FormData();
    formData.append('imageFile', file);
    this.projectService.upload(formData);*/
    this.projectForm.get(input).setValue(event.file.name);
  }

  saveProject(validate_proyect: false) {
    this.validateProjectForm(this.projectForm);
    if (this.projectForm.valid || !validate_proyect) {
      this.submitProject(validate_proyect);
    }
  }

  submitProject(validate_proyect: false) {
    this.projectSent = false;
    this.projectValidate = false;
    let params = this.projectForm.value;
    params['validate_proyect'] = validate_proyect;
    this.projectService.edit(params)
    .then(res => {
      if ( !res.error && res.status === true ) {
        this.userService.get();
        if (validate_proyect) {
          this.projectValidate = true;
          this.isReadonly = true;
        } else {
          this.projectSent = true;
        }
      }
    });
  }

  validateProjectForm(data?: any) {
    if (!data) { return; }
    const form = data;
    /*this.errorMessages = '';*/
    for (const field in this.projectFormErrors) {
      if (field) {
        this.projectFormErrors[field] = null;
        const control = form.get(field);
        if (control && !control.valid) {
          this.projectFormErrors[field] = true;
        }
      }
    }
    form.valueChanges
    .subscribe( res => this.onProjectValueChanged(data));
  }

  onProjectValueChanged(data?: any) {
    if (!data) { return; }
    const form = data;
    /*this.errorMessages = '';*/
    for (const field in this.projectFormErrors) {
      if (field) {
        // reset errors from input
        this.projectFormErrors[field] = null;
        const control = form.get(field);
        if (control && !control.valid) {
          this.projectFormErrors[field] = true;
        }
      }
    }
  }

}
