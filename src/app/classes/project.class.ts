export class Project {
  id: number;
  name: string;
  author: string;

  description: string;
  explanation: string;

  mainImage: string;
  profileImage: string;
  businessImage: string;
  additionalImage1: string;
  additionalImage2: string;
  additionalImage3: string;

  web: string;
  youtubeId: string;  
  
  creationDate: string;
  modificationDate: string;
  deletionDate: string;
  
  attachedFiles: string[];
  vote: boolean;
}
