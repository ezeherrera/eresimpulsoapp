import { Component } from '@angular/core';

declare var jQuery:any;

export class ResourceItem {
  title: string;
  description: string;
  url: string;
}

@Component({
  moduleId: module.id,
  selector: 'app-resources',
  templateUrl: 'resources.component.html',
  styleUrls: ['resources.component.scss']
})

export class ResourcesComponent {
  templates: ResourceItem[];
  manuals: ResourceItem[];
  limit: Number;

  constructor() {
	this.limit = 3;
  this.templates = [
      {
        title: 'EL PLAN FINANCIERO',
        description: 'Te ofrecemos una plantilla de plan financiero para que lo puedas cumplimentarlo según tu proyecto.',
        url: 'documents/Plan Financiero.xls'
      },
      {
        title: 'LIENZO DE MODELO DE NEGOCIO',
        description: 'Te ofrecemos un lienzo de modelo de negocio vacío para que puedas cumplimentarlo según tu proyecto.',
        url: 'documents/Lienzo_de_Modelo_de_Negocio.pdf'
      },
      {
        title: 'TEORÍA DEL CAMBIO',
        description: 'Te ofrecemos un modelo vacío de Teoría del Cambio para que puedas cumplimentarla según tu proyecto.',
        url: 'documents/Teoria_de_cambio.pdf'
      },
    ];

    this.manuals = [
      {
        title: 'MODELOS DE NEGOCIO SOCIALES',
        description: 'Un modelo de negocio describe la lógica por la cual una organización crea, aporta y captura valor.',
        url: 'documents/Lectura_4_Modelos_de_negocio_sociales.pdf'
      },
      {
        title: 'LIENZO DE MODELO DE NEGOCIO',
        description: 'Un modelo de negocio describe la lógica por la cual una organización crea, aporta y captura valor.',
        url: 'documents/Lectura_5_HERRAMIENTA_El_lienzo_de_modelo_de_negocio.pdf'
      },
      {
        title: 'TEORÍA DEL CAMBIO',
        description: 'Te explicamos qué es la Teoría de Cambio (TDC) y cómo desarrollar tu proyecto para cumplir tu objetivo de cambio.',
        url: 'documents/Lectura_1_Teoría_de_cambio.pdf'
      },
      {
        title: 'GUÍA PARA RELLENAR TU CANDIDATURA',
        description: 'Hemos elaborado esta pequeña guía que esperamos te resulte útil para aclarar qué esperamos en cada pregunta de tu candidatura al programa Eres Impulso.',
        url: 'documents/Guía_para_rellenar_la_candidatura_Eres_Impulso_Segunda_Edición.pdf'
      },
      {
        title: 'ELEMENTOS DE DIAGNÓSTICO DE UN PROBLEMA SOCIAL',
        description: 'El objetivo de este documento es ayudarte en el proceso, a veces largo y complicado, de elaborar un buen diagnóstico del problema social que quieres resolver con tu proyecto.',
        url: 'documents/Lectura_3_Elementos_de_diagnóstico_de_un_problema_social.pdf'
      },
      {
        title: 'CLAVES PARA COMUNICAR TU PROYECTO',
        description: 'A la hora de planificar la comunicación del proyecto es clave tener en cuenta el objetivo y la audiencia: ¿a quién vamos a comunicar y qué queremos conseguir?',
        url: 'documents/Lectura_6_Claves_para_comunicar_tu_proyecto.pdf'
      },
      {
        title: 'ANÁLISIS SISTÉMICO DE PROBLEMAS',
        description: 'En este documento veremos un caso de innovación social a partir de un análisis sistémico, y a continuación aportaremos algunas claves y herramientas para aplicar esta lógica a cualquier reto social.',
        url: 'documents/Lectura_2_Análisis_sistémico_de_problemas.pdf'
      },
    ];
  }
  
  seeMore(): void {    
	  this.limit = this.manuals.length;
  }
}