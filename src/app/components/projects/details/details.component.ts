import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from 'app/services/project.service';
import { Project } from 'app/classes/project.class';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'environments/environment';

import { UserService } from 'app/services/user.service';
import { VotingService } from 'app/services/voting.service';

declare var jQuery:any;

@Component({
  moduleId: module.id,
  selector: 'app-projectdetails',
  templateUrl: 'details.component.html',
  styleUrls: ['details.component.scss']
})

export class ProjectDetailsComponent implements OnInit, OnDestroy {
  public env = environment;
  public id: number;
  public sub: any;
  public project: Project[];
  public projects: Project[];
  public youtubeVideo: string;
  public carousel: string[];
  public project_real_image: string;
  public project_real_url: string;
  public project_url_name: string;
  public twitter: string;
  public facebook: string;
  public year: string;
  public status: string;
  public voted: Boolean = false;

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private router: Router,
    private user: UserService,
    private voting: VotingService
  ) {
    this.project = [];
    this.projects = [];
    this.carousel = [];
  }

  ngOnInit(): void {
    this.id = this.getIdFromURL(); // id from url

    // do init request
    this.projectService
      .get(this.id)
      .then(project => {
        this.project = project;
        this.youtubeVideo = project.project_youtube_id;
        this.year = project.project_year;
        this.status = project.project_status;

        localStorage.setItem('activeProject', project.project_name);

        // create dinamic carousel
        if (project.project_additional_image1) this.carousel.push(this.env.apiHost + 'images/projects/' + project.project_id + '/' + project.project_additional_image1);
        if (project.project_additional_image2) this.carousel.push(this.env.apiHost + 'images/projects/' + project.project_id + '/' + project.project_additional_image2);
        if (project.project_additional_image3) this.carousel.push(this.env.apiHost + 'images/projects/' + project.project_id + '/' + project.project_additional_image3);

        this.project_url_name = project.project_name
          .toLowerCase()
          .replace(/ /g,'-')
          .replace(/[^\w-]+/g,'');

        // create urls
        this.project_real_image = this.env.apiHost + 'images/projects/' + project.project_id + '/' + project.project_main_image;       
        this.project_real_url   = this.env.apiHost + 'projects/' + project.project_id + '/' + this.project_url_name;            
        this.twitter            = 'https://twitter.com/share?text=' + project.project_name + '&url=' + this.project_real_url + '&hashtags=eresimpulso2017&via=aguafontvella';                
        this.facebook           = 'https://www.facebook.com/sharer/sharer.php?t=' + project.project_name + '&u=' + this.project_real_url;

        // add embed video
        if (this.youtubeVideo) {
          jQuery('#youtube-video').html('<iframe width="560" height="315" src="https://www.youtube.com/embed/' + this.youtubeVideo + '" frameborder="0" allowfullscreen></iframe>');
        }

        // init owl carousel
        jQuery(document).ready(function (){
          jQuery('.owl-carousel').owlCarousel({
            loop:   false,
            margin: 0,
            nav:    false,
            // lazyLoad: true,
            // autoHeight: true,
            responsive:{
              0:{
                items: 1
              },
              640:{
                items: 1
              },
              1024:{
                items: 1
              }
            }
          });
        });

        // update project voting status
        if (this.voting.user['votes']) {
          this.voted = ( this.voting.user['votes'].indexOf(project.project_id) >= 0 ) ? true : false;
        }
        this.voting.onUserLoged.subscribe( data => {
          this.voted = ( this.voting.user['votes'].indexOf(project.project_id) >= 0 ) ? true : false;
        });

      });

  }

  ngOnDestroy() {
    localStorage.removeItem('activeProject');
    this.sub.unsubscribe();
  }

  private getIdFromURL(): number {
    this.sub = this.route.params.subscribe(params => {
       this.id = + params['project_id'];
    });
    return this.id;
  }

  private getList(year: string, next: boolean): void {
    this.projectService
      .getList(year)
      .then(projects => {
        this.navigate(projects, year, next);
      });
  }

  public getNavigate(year: string, status: string, next: boolean) :void {   
    if (!year || year === '2017') this.getList(year, next);
    else                          this.getListFromJSON(year, status, next);    
  }

  public getSeeAll(year: string) :void {                 
    if (!year || year === '2017') this.router.navigate(['/proyectos']);
    else                          this.router.navigate(['/ediciones-anteriores']);
  }

  private getListFromJSON(year: string, status: string, next: boolean): void {   
    if (! status) {
      this.router.navigate(['/ediciones-anteriores']);
    }

    let projects = this.getFinalistasProject(year, status, next);      
    this.navigate(projects, year, next);
  }

  private getFinalistasProject(year: string, status: string, next: boolean): string[]{
    let projects = [];    

    if (status == 'Ganador')        projects = year === '2015' ? this.projectService.ganadoras2015 : this.projectService.ganadoras2016;
    else if (status == 'Finalista') projects = year === '2015' ? this.projectService.finalistas2015 : this.projectService.finalistas2016;

    return projects;    
  }  

  private navigate(projects: any, year: string, next: boolean): void {
    let project: any;
    let id: string;
    let position: number;
    let link: string;
    let description: string;

    id        = this.id.toString();
    link      = '';
    position  = projects.map(function(e) { return e.project_id; }).indexOf(id);

    if (next){
      if(position >= projects.length - 1) {
        project = projects[0];
      }else{
        project = projects[position + 1];
      }
    }else{
      if(position <= 0) {
        project = projects[projects.length - 1];
      }else{
        project = projects[position - 1];
      }
    }

    console.log(project);

    description = project.project_name
      .toLowerCase()
      .replace(/ /g,'-')
      .replace(/[^\w-]+/g,'');

    link = '/proyectos/' + project.project_id + '/' + description;
    // this.router.navigate ([link]);
    window.location.href = link;
  }

  public vote(item) {
    if (!this.voting.user['email']) {
        // open social login modal
       jQuery('#social-login').foundation('open');
       // listen to social login event
       this.voting.onUserLoged.subscribe( data => {
          this.voted = ( this.voting.user['votes'].indexOf(item.project_id) >= 0 ) ? true : false;
          // if project is not voted
          if (!this.voted) {
            // call vote method again
            this.vote(item);
          }
          // close social login modal after user is loged
          jQuery('#social-login').foundation('close');
        });
    } else {
      const params = {
        user: this.voting.user['email'],
        project: item.project_id
      };
      this.voting.vote(params)
      .then( data => {
        if (data.status === true ) {
          this.project['votes'].push(this.voting.user['email']);
          this.voted = true;
        }
        // update user data
        this.voting.setUser(this.voting.user['email']);
      } );
    }
  }

}
