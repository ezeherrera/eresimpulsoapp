import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'app/services/project.service';
import { Project } from 'app/classes/project.class';
// import { Navs } from 'app/classes/navs.class';

import { PagerService } from 'app/services/pager.service';

@Component({
  moduleId: module.id,
  selector: 'app-projectlist',
  templateUrl: 'list.component.html',
  styleUrls: ['list.component.scss']
})

export class ProjectListComponent implements OnInit {
  // navs: Navs[];
  private allItems: any[];
  public pager: any = {};      
  public projects: Project[];
  public currentYear: number;
  // public limit: number;
  public order: {
    field: string;
    direction: string;
  };

  constructor(
    private projectService: ProjectService,
    private pagerService: PagerService
  ){
    this.currentYear = 2017;
    // this.limit = 2;
    this.projects = [];
    this.order = {
      field: 'creationDate',
      direction: 'asc'
    };
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    this.pager    = this.pagerService.getPager(this.allItems.length, page);
    this.projects = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  setPageMobile(page: number): void {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    this.pager    = this.pagerService.getPager(this.allItems.length, page);
    this.projects = this.allItems.slice(0, this.pager.endIndex + 1);  
  }

  ngOnInit(): void {
    this.projectService
      .getList(this.currentYear)
      .then(projects => {
        this.projects = projects;
        this.allItems = projects;
        this.setPage(1);
      });
  }
}
