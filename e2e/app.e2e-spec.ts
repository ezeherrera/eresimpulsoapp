import { EresimpulsoAppPage } from './app.po';

describe('eresimpulso-app App', () => {
  let page: EresimpulsoAppPage;

  beforeEach(() => {
    page = new EresimpulsoAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
