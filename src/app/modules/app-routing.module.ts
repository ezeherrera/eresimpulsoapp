import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router, NavigationEnd, ActivatedRoute, Params } from '@angular/router';

import { AuthGuard } from 'app/guard/auth.guard';
import { HomeComponent } from 'app/components/home/home.component';
import { LoginComponent } from 'app/components/login/login.component';
import { RegisterComponent } from 'app/components/register/register.component';
import { UserAreaComponent } from 'app/components/user-area/user-area.component';
import { ProjectFormComponent } from 'app/components/projects/form/project-form.component';
import { ContactComponent } from 'app/components/contact/contact.component';
import { ProjectListComponent } from 'app/components/projects/list/list.component';
import { ProjectDetailsComponent } from 'app/components/projects/details/details.component';
import { ProjectFinalistasComponent } from 'app/components/projects/finalistas/finalistas.component';
import { AboutComponent } from 'app/components/about/about.component';
import { InspireComponent } from 'app/components/inspire/inspire.component';
import { ResourcesComponent } from 'app/components/resources/resources.component';
import { FaqsComponent } from 'app/components/faqs/faqs.component';

import { GtmService } from 'app/services/gtm.service';

declare var jQuery: any;

const routes: Routes = [
  { path: '',                         component: HomeComponent },
  { path: 'iniciar-sesion',           component: LoginComponent, canActivate: [AuthGuard]},
  { path: 'registro',                 component: RegisterComponent },
  { path: 'area-usuario',             component: UserAreaComponent , canActivate: [AuthGuard]},
  { path: 'area-usuario/:tab',        component: UserAreaComponent },
  { path: 'validacion_registro/:id',  component: RegisterComponent },
  { path: 'contacto',                 component: ContactComponent },
  { path: 'que-es-eresimpulso',       component: AboutComponent },
  { path: 'proyectos',                component: ProjectListComponent },
  { path: 'ediciones-anteriores',     component: ProjectFinalistasComponent },
  { path: 'proyectos/:project_id/:title',     component: ProjectDetailsComponent },
  { path: 'te-asesoramos',            component: ResourcesComponent },
  { path: 'inspirate',                component: InspireComponent },
  { path: 'sube-tu-proyecto',         component: ProjectFormComponent, canActivate: [AuthGuard]},
  { path: 'faqs',                     component: FaqsComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private gtm: GtmService
  ) {

      router.events.subscribe( event => {
        if (event instanceof NavigationEnd) {
          /* Etiquetado Transversal */
          this.gtm.transversalPush();
          //jQuery(document).foundation();
        }
        // NavigationStart
        // NavigationEnd
        // NavigationCancel
        // NavigationError
        // RoutesRecognized
      });
  }

}
