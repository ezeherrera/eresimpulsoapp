import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'environments/environment';

import { UserService } from 'app/services/user.service';
import { VotingService } from 'app/services/voting.service';

declare var jQuery: any;

@Component({
  moduleId: module.id,
  selector: 'project-item',
  templateUrl: 'project-item.component.html',
  styleUrls: ['project-item.component.scss']
})

export class ProjectItemComponent implements OnInit {
  @Input() item;
  public env = environment;
  public project_real_url: string;
  public project_real_image: string;
  public project_url_name: string;
  public project_description_truncated: string;
  public twitter: string;
  public facebook: string;
  public voted: Boolean = false;

  constructor(
      private user: UserService,
      private voting: VotingService
  ) {}

  ngOnInit() {
    // truncate description
    this.project_description_truncated = this.getTruncateText(this.item.project_description, 200);
    // url name
    this.project_url_name = this.item.project_name
      .toLowerCase()
      .replace(/ /g,'-')
      .replace(/[^\w-]+/g,'');

    // url
    this.project_real_url = this.env.apiHost + 'projects/' + this.item.project_id + '/' + this.project_url_name;
    // url image
    this.project_real_image = this.env.apiHost + 'images/projects/' + this.item.project_id + '/' + this.item.project_main_image;
    // twitter
    this.twitter = 'https://twitter.com/share?text=' + this.item.project_name +
                   '&url=' + this.project_real_url +
                   '&hashtags=eresimpulso2017&via=aguafontvella';
    // facebook
    this.facebook = 'https://www.facebook.com/sharer/sharer.php?t=' + this.item.project_name + '&u=' + this.project_real_url;

    // update project voting status
    if (this.voting.user['votes']) {
      this.voted = ( this.voting.user['votes'].indexOf(this.item.project_id) >= 0 ) ? true : false;
    }
    this.voting.onUserLoged.subscribe( data => {
      this.voted = ( this.voting.user['votes'].indexOf(this.item.project_id) >= 0 ) ? true : false;
    });
  }

  getTruncateText(text: string, limit: number) {
    let trimmed: string;

    trimmed = text.substr(0, limit);
    trimmed = trimmed.substr(0, Math.min(trimmed.length, trimmed.lastIndexOf(' ')));
    trimmed += '...';

    return trimmed;
  }

  public vote(item) {
    if (!this.voting.user['email']) {
        // open social login modal
       jQuery('#social-login').foundation('open');
       // listen to social login event
       this.voting.onUserLoged.subscribe( data => {
          this.voted = ( this.voting.user['votes'].indexOf(item.project_id) >= 0 ) ? true : false;
          // if project is not voted
          if (!this.voted) {
            // call vote method again
            this.vote(item);
          }
          // close social login modal after user is loged
          jQuery('#social-login').foundation('close');
        });
    } else {
      const params = {
        user: this.voting.user['email'],
        project: item.project_id
      };
      this.voting.vote(params)
      .then( data => {
        if (data.status === true ) {
          this.item.votes.push(this.voting.user['email']);
          this.voted = true;
        }
        // update user data
        this.voting.setUser(this.voting.user['email']);
      } );
    }
  }
}
