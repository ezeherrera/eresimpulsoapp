import { Component, Input, ElementRef, AfterViewInit, OnInit} from '@angular/core';
import { User } from 'app/classes/user.class';
import { UserService } from 'app/services/user.service';

declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss']
})


export class HeaderComponent implements AfterViewInit, OnInit {
  @Input()
  user:  User = new User;

  constructor(
    private el: ElementRef,
    private userService: UserService
  ) {}

  ngOnInit() {
    //if ( localStorage.getItem('currentUser') ) {
      this.userService.get();
    //}
    this.updatedUser();
  }

  updatedUser(): void {
    this.userService.updatedUser.subscribe(
      (user) => {
        if (typeof user !== 'undefined') {
          this.user = this.userService.currentUser;
        }else {
          // reset user var
          this.user = new User;
        }
      }
    );
  }

  ngAfterViewInit() {
    // init local foundation
    jQuery(this.el.nativeElement.localName).foundation();

    // toggle click class
    jQuery('#menu-icon').on('click', function () {
      jQuery(this).toggleClass('menu-icon-open');
    });
  }
}
