import { Component, ElementRef } from '@angular/core';

declare var jQuery:any;

export class FaqQuestion {
  id: number;
  question: string;
  answer: string;
}

@Component({
  selector: 'app-faqs',
  templateUrl: 'faqs.component.html',
  styleUrls: ['faqs.component.scss']
})

export class FaqsComponent {
  about: FaqQuestion[];
  projects: FaqQuestion[];
  selectedQuestion: FaqQuestion;
  row = {};

  constructor(
    private el: ElementRef
  ) {
    this.about = [
      {
        id: 1,
        question: '¿Qué es Eres Impulso?',
        // tslint:disable-next-line:max-line-length
        answer: 'Es una plataforma de emprendimiento femenino que la marca de agua mineral natural Font Vella creó hace tres años con el objetivo de  ayudar a todas aquellas mujeres que tienen una idea y no saben cómo llevarla a cabo o tienen un proyecto que necesita un empujón para crecer. En Eres Impulso las participantes no solo podrán dar visibilidad a su proyecto a través de distintas herramientas de comunicación sino que también podrán conseguir el apoyo de Font Vella para darle ese impulso que necesita y conocer a otras emprendedoras como ellas.<br>10 finalistas, 9 de ellas elegidas por un jurado experto y una a través de votación popular, recibirán orientación y formación para impulsar su proyecto. De entre estas, 2 serán las emprendedoras que ganen  por elección del jurado y las que reciban el apoyo económico de Font Vella, además del asesoramiento del jurado en el desarrollo o lanzamiento de su proyecto.<br>En esta tercera edición, como en las anteriores, Font Vella busca impulsar proyectos sociales, con o sin ánimo de lucro, que tengan un impacto local.'
      },
      {
        id: 2,
        question: '¿Por qué Font Vella ha creado Eres Impulso?',
        // tslint:disable-next-line:max-line-length
        answer: 'Font Vella es una marca que siempre ha estado al lado de las  mujeres, impulsándolas, ayudándolas e hidratándolas porque cuando las mujeres se sienten bien  son imparables.<br>Ahora, además de nutrirlas de bienestar, también quiere alimentar su mayor poder: hacer que el mundo avance. Por eso, para apoyar sus ganas de hacer que el mundo avance, la marca ha creado una plataforma de apoyo en la que las participantes no solo podrán impulsar sus ideas, sino que también podrán recibir el asesoramiento de expertos y vivir una experiencia única al lado de otras mujeres emprendedoras.'
      }
    ];

    this.projects = [
      {
        id: 3,
        question: '¿Qué tengo que hacer para presentar un proyecto en Eres Impulso?',
        // tslint:disable-next-line:max-line-length
		answer: 'Desde la página principal <a href="https://www.eresimpulso.fontvella.es">www.eresimpulso.fontvella.es</a> puedes acceder a “Empezar” para crear tu proyecto. Allí podrás registrarte como usuario de la plataforma y subir el material de tu proyecto para que lo validemos. Una vez validado y entregado a nuestro jurado, tu proyecto aparecerá publicado en la Galería, donde podrás compartirlo en las redes sociales para dar a conocer tu idea con las personas que tú quieras y conseguir que éstas lo voten.'
      },
      {
        id: 4,
        question: '¿Quién puede presentar proyectos?',
        // tslint:disable-next-line:max-line-length
        answer: 'Cualquier mujer mayor de 18 años, española o residente en España.'
      },
      {
        id: 5,
        question: '¿Qué tipo de proyectos se pueden presentar?',
        // tslint:disable-next-line:max-line-length
        answer: 'En Eres Impulso entendemos que hay proyectos que están en distintas fases de evolución  y por eso puedes presentar una idea que necesita apoyo para llevarse a cabo o un proyecto real que necesitan un empujón para crecer.'
      },
      {
        id: 6,
        question: '¿Qué es un proyecto social con impacto local?',
        // tslint:disable-next-line:max-line-length
        answer: 'Es todo  proyecto que busca generar un impacto positivo en nuestro alrededor y entre las personas y colectivos que viven en él, ya sean niños, empresas, estudiantes, tercera edad u otros. Su objetivo principal es hacer que su entorno avance de un modo sostenible y respetuoso.'
      },
      {
        id: 7,
        question: '¿Dónde puedo encontrar ejemplos de proyectos sociales con impacto local para saber si el mío lo es?',
        // tslint:disable-next-line:max-line-length
        answer: 'Puedes echar un vistazo a la Galería de proyectos de las ediciones anteriores para ver el tipo de propuestas que se están presentando. Podrás consultar los proyectos ganadores y los finalistas. ¡Seguro que te será de mucha utilidad!'
      },
      {
        id: 8,
        question: '¿De cuánto tiempo dispongo para presentar mi proyecto?',
        // tslint:disable-next-line:max-line-length
        answer: 'El período de inscripción empieza el 27 de abril y tienes hasta el 30 de junio de 2017 para presentar tu propuesta. No se admitirán proyectos después de las fechas señaladas.'
      },
      {
        id: 9,
        question: '¿En qué formato debo presentar mi proyecto?',
        // tslint:disable-next-line:max-line-length
        answer: 'En PDF o Word, como tú prefieras. En la web dispones de una plantilla, creada por Ship2B, que deberás utilizar para presentar tu plan de empresa. Junto con el documento que subas a la hora de inscribirte, también podrás adjuntar material extra como imágenes o links a YouTube o a la web de tu proyecto, si la tienes.'
      },
      {
        id: 10,
        question: '¿Puedo presentar varios proyectos?',
        // tslint:disable-next-line:max-line-length
        answer: 'No, solamente puedes presentar un proyecto.'
      },
      {
        id: 11,
        question: '¿Puedo editar mi proyecto una vez publicado en la plataforma?',
        // tslint:disable-next-line:max-line-length
        answer: 'Si tu proyecto está pendiente de aprobación o ya está aprobado, no puedes modificarlo, pero lo puedes eliminar y volver a enviar con los cambios que hayas hecho.<br>Solo si tu proyecto ha sido rechazado en la fase previa a la publicación, podrás editarlo y corregir los puntos que te pida el jurado.'
      },
      {
        id: 12,
        question: '¿Quién se encargará de evaluar mi proyecto?',
        // tslint:disable-next-line:max-line-length
        answer: 'Un jurado experto será el encargado de evaluar las propuestas presentadas:<ul><li>Sonia Navarro - Directora del Instituto para la Innovación Social de ESADE</li><li>Clara Navarro - Cofundadora de Ship2B</li>  <li>Aurelie Salvaire - Fundadora de Shiftbalance</li><li>Alexandra Mitjans – Directora de Relaciones Institucionales de Ashoka España y Directora de Ashoka Cataluña.</li>  <li>Pablo Sánchez - Socio co-fundador de Roots for Sustainability y Director de B Corp en España.</li></ul>'
      },
      {
        id: 13,
        question: '¿Qué criterios se tendrán en cuenta a la hora de evaluar mi proyecto?',
        // tslint:disable-next-line:max-line-length
        answer: 'Los principales criterios que se evaluarán serán el impacto social, la sostenibilidad, la viabilidad, la escalabilidad del proyecto y el carácter emprendedor de la concursante.'
      },
      {
        id: 14,
        question: '¿Cómo funciona el proceso de votación? ¿Quién puede votar?',
        // tslint:disable-next-line:max-line-length
        answer: 'Cualquier usuario que acceda a la web podrá votar los proyectos que crea más interesantes para entrar en la final de Eres Impulso. Cada proyecto tan sólo podrá recibir 1 voto por parte de cada usuario. El proyecto más votado será uno de los finsalistas y podrá optar a ser uno de los ganadores. Por ello, te animamos a que compartas tu candidatura a todas tus amistades y familiares con el fin de conseguir muchos votos. ¡Suerte!'
      },
      {
        id: 15,
        question: '¿Cuál es el período de votación?',
        // tslint:disable-next-line:max-line-length
        answer: 'El período de votación empieza desde el primer día en que se pueden subir proyectos (27 de abril) hasta el 15 de julio de 2017. No se podrán votar proyectos de ediciones anteriores ni fuera de las fechas señaladas.'
      },
      {
        id: 16,
        question: '¿Qué puedo conseguir en Eres Impulso?',
        // tslint:disable-next-line:max-line-length
        answer: 'Como finalista:<ul><li>Una jornada de formación consistente en un taller de formación en Storytelling por Aurélie Salvaire y un taller de Lean Start-Up impartido por Ship2B</li><li>Hacer networking con otras emprendedoras como tú.</li><li>¡Vivir una experiencia única!</li></ul>Como ganadora:<ul><li>10.000€ para impulsar tu proyecto.</li><li>Asesoramiento y apoyo en el desarrollo y lanzamiento de tu proyecto por parte de Ship2B.</li></ul>'
      },
      {
        id: 17,
        question: '¿Cuántos proyectos recibirán el apoyo de Font Vella?',
        // tslint:disable-next-line:max-line-length
        answer: 'De entre todas las propuestas presentadas, el jurado seleccionará 10 proyectos, de entre los cuales, en una segunda fase, elegirá aquellos 2 que considere de mayor relevancia social. Estos 2 proyectos serán los que reciban el apoyo económico de Font Vella.'
      },
      {
        id: 18,
        question: 'Mi proyecto ha conseguido el objetivo de financiación. ¿Cómo recibo el dinero?',
        // tslint:disable-next-line:max-line-length
        answer: 'Tras ser seleccionada como una de los 2 ganadoras de Eres Impulso, Font Vella te hará entrega de 10.000 euros para dar viabilidad a tu plan de negocio.'
      },
      {
        id: 19,
        question: '¿Font Vella se queda con alguna parte de los derechos o propiedad intelectual de las propuestas financiadas a través de su plataforma?',
        // tslint:disable-next-line:max-line-length
        answer: 'No, las emprendedoras siempre conservan el 100% de los derechos y la propiedad sobre sus proyectos.'
      },
      {
        id: 20,
        question: 'En caso de que escojan mi proyecto como ganador, ¿puede a partir de entonces Font Vella pedirme que modifique algún apartado de mi proyecto?',
        // tslint:disable-next-line:max-line-length
        answer: 'No, siempre se respetará el proyecto tal y como ha sido presentado, una condición que el candidato ganador también deberá mantener. Si ganas, lo que sí hará Font Vella es poner a tu disposición expertos de emprendimiento social que te asesorarán sobre el proyecto, su potencialidad y sus posibles mejoras.'
      },
      {
        id: 21,
        question: '¿Los proyectos recibirán algún tipo de publicidad?',
        // tslint:disable-next-line:max-line-length
        answer: 'Todos los proyectos presentados estarán disponibles en la plataforma <a href="https://www.eresimpulso.es">www.eresimpulso.es</a> pero además, Font Vella dará a conocer los proyectos seleccionados a través de sus redes sociales, nota de prensa, base de datos y página web.'
      },
      {
        id: 22,
        question: '¿Puedo parar mi proyecto y cancelar el proceso de participación?',
        // tslint:disable-next-line:max-line-length
        answer: 'Sí, si por la razón que sea no puedes llevar a cabo tu proyecto, puedes cancelar el proceso de participación.<br>Una opción que siempre tendrás disponible en el apartado de edición de datos personales, donde solo tendrás que borrar tu proyecto.'
      },
      {
        id: 23,
        question: 'Si creo que no puedo llevar a cabo el proyecto, ¿qué tengo que hacer?',
        // tslint:disable-next-line:max-line-length
        answer: 'Si por algún motivo crees que no podrás seguir adelante con tu propuesta, deberás cerrar el proyecto. En el caso de que ya hubieras recibido la financiación, deberás devolver el dinero.'
      },
    ];

    this.selectedQuestion = null;
    this.row = {
      about: Array.from(Array(Math.ceil(this.about.length / 3)).keys()),
      projects: Array.from(Array(Math.ceil(this.projects.length / 3)).keys()),
    };
  };

  toggle(question: FaqQuestion): void {
    if ( question === this.selectedQuestion ) {
      this.close();
    } else {
      this.show(question);
    }
  }

  show(question: FaqQuestion): void {
    this.selectedQuestion = question;
  }

  close(): void {
    this.selectedQuestion = null;
  }

  ngAfterViewInit() {
    // init local foundation
    jQuery(this.el.nativeElement.localName).foundation();
  }
}
