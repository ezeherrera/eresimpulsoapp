import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, NG_VALIDATORS } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { UserService } from 'app/services/user.service';
import { ProjectService } from 'app/services/project.service';
import { GtmService } from 'app/services/gtm.service';

import { ImageUploadComponent } from 'app/modules/ng2-image-upload/src/image-upload/image-upload.component';

declare var jQuery: any;

@Component({
  moduleId: module.id,
  selector: 'app-project-form',
  templateUrl: 'project-form.component.html',
  styleUrls: ['project-form.component.scss']
})

export class ProjectFormComponent {
  projectForm: FormGroup;
  uploadImageUrl = `${this.projectService.env.apiHost}api/uploadImage`;
  step = 1;
  question = 1;
  questions = [ 1 , 2 , 3 , 4 , 5 , 6 ];
  formErrors = {
    'name': '',
    'description': '',
    'profile_image': '',
    'main_image': ''
  };
  project_id: Number;
  validationMessage: Boolean = false;

  constructor(
    private userService: UserService,
    private projectService: ProjectService,
    private fb: FormBuilder,
    private gtm: GtmService
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.projectForm = this.fb.group({
      project_id:                [''],
      step1: this.fb.group({
              name:              ['', [Validators.required, Validators.maxLength(40)]],
              description:       ['', [Validators.required, Validators.maxLength(300)]],
              profile_image:     ['', Validators.required],
              main_image:        ['', Validators.required],
              web:               [''],
              additional_image1: [''],
              additional_image2: [''],
              additional_image3: [''],
              explanation:       ['']
            }),
      step2: this.fb.group({
              explanation:      ['', [Validators.required, Validators.maxLength(1024)]],
              model_business:   ['', [Validators.required, Validators.maxLength(1024)]],
              team_description: ['', [Validators.required, Validators.maxLength(1024)]],
              calendar:         ['', [Validators.required, Validators.maxLength(1024)]],
              need_economic:    ['', [Validators.required, Validators.maxLength(1024)]],
              info_extra:       ['', [Validators.required, Validators.maxLength(1024)]],
            })
    });
  }

  save(validate_proyect: false) {
    this.validateForm(this.projectForm.get('step1'));
    if (!this.projectForm.get('step1').valid && this.step === 2) {
      this.step = 1;
    } else {
      this.submit(validate_proyect);
    }
  }

  submit(validate_proyect: boolean = false) {
    if (this.projectForm.get('project_id').value) {
      let params = {};
      const step1 = this.projectForm.get('step1').value;
      const step2 = this.projectForm.get('step2').value;
      // tslint:disable-next-line:forin
      params = step1;
      // tslint:disable-next-line:forin
      for (const attrname in step2) { params[attrname] = step2[attrname]; }
      params['project_id'] = this.projectForm.get('project_id').value;
      params['validate_proyect'] = validate_proyect;
      console.log(params);

      this.projectService.edit(params)
      .then(res => {
        if (!res.error && res.status === true ) {
          if (validate_proyect) {
            this.step = 3;
          } else {
            this.validationMessage = true;
          }
        }
      });
    } else {
      this.nextStep(true);
    }
  }

  nextStep(draft?: boolean) {
    this.validateForm(this.projectForm.get('step1'));
    if (this.projectForm.get('step1').valid) {
      const params = this.projectForm.get('step1').value;
      if (!this.projectForm.get('project_id').value) {
        this.projectService.create(params)
          .then(res => {
            if (typeof res.project_id !== 'undefined') {
              this.projectForm.get('project_id').setValue(res.project_id);
              this.userService.get();

              const _params = {
                'event': 'user_action',
                'event_category': 'accion usuario',
                'event_action': 'subir contenido',
                'event_label': this.userService.currentUser.id
              }; this.gtm.push(_params);
            }
          });
      }
      if (!draft) {
        this.validationMessage = false;
        this.step = 2;
      } else {
        this.validationMessage = true;
      }
    }
  }

  backStep() {
    this.validationMessage = false;
    this.step = 1;
  }

  validateForm(data?: any) {
    if (!data) { return; }
    const form = data;
    for (const field in this.formErrors) {
      if (field) {
        this.formErrors[field] = null;
        const control = form.get(field);
        if (control && !control.valid) {
          this.formErrors[field] = true;
        }
      }
    }
    form.valueChanges
    .subscribe( res => this.onValueChanged(data));
  }

  onValueChanged(data?: any) {
    if (!data) { return; }
    this.validationMessage = false;
    const form = data;
    for (const field in this.formErrors) {
      if (field) {
        // reset errors from input
        this.formErrors[field] = null;
        const control = form.get(field);
        if (control && !control.valid) {
          this.formErrors[field] = true;
        }
      }
    }
  }

  gotToQuestion(index: number) {
    if (!index) { return; }
    if (index <= this.questions.length && index > 0) {
      this.question = index;
    }
  }

  imageUploaded( event, input: any ) {
    /*const file = event.file;
    const formData = new FormData();
    formData.append('imageFile', file);
    this.projectService.upload(formData);*/
    this.projectForm.get(input).setValue(event.file.name);
  }

  imageRemoved( event, input: string ) {
    this.projectForm.get(input).setValue('');
  }

  ngAfterViewInit() {
    // init owl carousel
    jQuery('.owl-carousel').owlCarousel({
      loop:   false,
      margin: 50,
      nav:    true,
      navText: ['<span class="icon-arrow"></span>', '<span class="icon-arrow"></span>'],
      responsive:{
        0:{
            items:1
        },
        640:{
            items:3
        },
        1024:{
            items:3
        }
      }
    })
  }
}
