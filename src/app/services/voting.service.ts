import { Injectable, Output, EventEmitter } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { UserService } from 'app/services/user.service';
import { AuthService } from 'angular2-social-login';
import { environment } from 'environments/environment';

@Injectable()
export class VotingService {

  public user: Object = {};
  private env = environment;
  private headers = new Headers({'Content-Type': 'multipart/form-data'});

  @Output() onUserLoged = new EventEmitter<Object>();

  constructor(
    public http: Http,
    public _auth: AuthService,
    public userService: UserService
  ) {
    // sign user with EI Session
    this.userService.updatedUser.subscribe( user => {
      if (typeof user !== 'undefined') {
        this.setUser(user.email);
      };
    });
  }

  signIn(provider) {
    const sub = this._auth.login(provider).subscribe(
      (data) => {
          this.setUser(data['email']);
        }
    );
  }

  setUser(email) {
    this.user['email'] = email;
    this.getUser({ user: email })
    .then( data => {
        this.user['votes'] = data.list;
        this.onUserLoged.emit(this.user);
      });
  }

  getUser(data): Promise<any> {
    const url = this.env.apiHost + 'api/voteList';
    return this.http
      .post(url, JSON.stringify(data), { headers: this.headers })
      .toPromise()
      .then( response => response.json() )
      .catch(this.handleError);
  }

  vote(data): Promise<any> {
    const url = this.env.apiHost + 'api/voteProject';
    return this.http
      .post(url, JSON.stringify(data), { headers: this.headers })
      .toPromise()
      .then( response => response.json() )
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
