import { Component, Input, ElementRef } from '@angular/core';

declare var jQuery: any;

@Component({
  moduleId: module.id,
  selector: 'inspire-navigation',
  templateUrl: 'inspire-navigation.component.html',
  styleUrls: ['inspire-navigation.component.scss']
})

export class InspireNavigationComponent {
  @Input() navs;
  @Input() action: Function;

  constructor(
    private el: ElementRef
  ){

  }  

  ngAfterViewInit() {
    // init local foundation
    jQuery(this.el.nativeElement.localName).foundation();
  }
}
