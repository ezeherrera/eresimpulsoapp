import { TestBed, inject } from '@angular/core/testing';

import { GtmService } from './gtm.service';

describe('GtmService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GtmService]
    });
  });

  it('should ...', inject([GtmService], (service: GtmService) => {
    expect(service).toBeTruthy();
  }));
});
