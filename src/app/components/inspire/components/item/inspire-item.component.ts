import { Component, Input, OnInit } from '@angular/core';

declare var jQuery:any;

@Component({
  moduleId: module.id,
  selector: 'inspire-item',
  templateUrl: 'inspire-item.component.html',
  styleUrls: ['inspire-item.component.scss']
})

export class InspireItemComponent implements OnInit {  
  @Input() item;    
  public title: string;

  constructor(    
  ){
    
  }

  getIcon() {   
    let type = this.item.type.toLowerCase();

    if (type === "video"){
       return "icon-videos";
    }

    if (type === "articulo"){
       return "icon-articulos";
    }

    if (type === "charla"){
       return "icon-charlas";
    }
    
    return "icon-circle";
  }    

  ngOnInit() {
    this.title = this.getTruncateText(this.item.title, 40);
  }  

  getTruncateText(text: string, limit: number) {         
    let trimmed: string;
    
    trimmed = text.substr(0, limit);    
    trimmed = trimmed.substr(0, Math.min(trimmed.length, trimmed.lastIndexOf(" ")));
    trimmed += '...';
    
    return trimmed;
  }

  openModal () {    
    let iframe = '<div class="responsive-embed"><iframe width="640" height="360" src="https://www.youtube.com/embed/' + this.item.videoHash + '" frameborder="0" allowfullscreen></iframe></div>';    
    jQuery("#app-modal-content").html(iframe);
  }  
}
