import { Component } from '@angular/core';
import { Navs } from 'app/classes/navs.class';

declare var jQuery:any;

export class InspireItem {
  type:   string;
  title:  string;
  author: string;
  source: string;
  image:  string;
  url:    string;
  video:  string;
  videoHash: string;
}

const limit = 12;
const items = [
  { 
    type: 'articulo',
    name: 'Artículo',
    title: 'Mujeres emprendedoras',
    author: 'Cristina Sáez',
    source: 'La Vanguardia',
    
    image: './assets/images/common/iStock-467646122.jpg',
    url: 'http://www.lavanguardia.com/estilos-de-vida/20130201/54363299065/mujeres-emprendedoras.html',
    video: '',
    videoHash: ''
  },  
  { 
    type: 'articulo',
    name: 'Artículo',
    title: 'Errores a evitar en el Plan de Negocio',
    author: 'Javier Escudero',
    source: 'Emprendedores.es',
    
    image: './assets/images/common/iStock-478671289.jpg',
    url: 'http://www.emprendedores.es/crear-una-empresa/plan-de-negocios-errores-a-evitar',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: 'Impulsar tu networking',
    author: 'Emprendedores.es',
    source: 'Emprendedores.es',
    
    image: './assets/images/common/iStock-500992796.jpg',
    url: 'http://www.emprendedores.es/gestion/ideas-mejorar-networking-contactos-comerciales',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: 'Analiza la viabilidad de tu idea',
    author: 'Emprendedores.es',
    source: 'Emprendedores.es',
    
    image: './assets/images/common/iStock-508388622.jpg',
    url: 'http://www.emprendedores.es/crear-una-empresa/idea-negocio-viable',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: '¿Qué es el emprendimiento social?',
    author: 'Angela Bernardo',
    source: 'Blogthinkingbig.com',
    
    image: './assets/images/common/iStock-521535828.jpg',
    url: 'http://blogthinkbig.com/emprendimiento-social/',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: 'Saber detectar las oportunidades de mercado para emprender',
    author: 'Sergio Gómez',
    source: 'El Referente',
    
    image: './assets/images/common/iStock-541836000.jpg',
    url: 'http://www.elreferente.es/innovadores/73-5--emprendedores-espanoles-inician-negocios-oportunidades-mercado-29314',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: '¿Cómo ser un inversor social?',
    author: 'Sergio Gómez',
    source: 'El Referente',
    
    image: './assets/images/common/iStock-598154358.jpg',
    url: 'http://www.elreferente.es/sociales/cinco-claves-para-convertirse-en-un-inversor-de-impacto-social-en-2016-29181',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: '¿Emprenden más mujeres o hombres?',
    author: 'World Economic Forum',
    source: '',
    
    image: './assets/images/common/iStock-467646122.jpg',
    url: 'https://www.weforum.org/agenda/2015/12/why-are-women-less-likely-to-be-entrepreneurs-than-men/?utm_content=buffer04773&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: 'La fuerza, con las mujeres emprendedoras en 2016',
    author: 'Geri Stengel',
    source: 'Forbes',
    
    image: './assets/images/common/iStock-509051584.jpg',
    url: 'https://www.forbes.com/sites/geristengel/2016/01/06/why-the-force-will-be-with-women-entrepreneurs-in-2016/#7db96fb14f8b',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: 'La eco-comunicación, también para emprendedores',
    author: 'Indra Kishinchand',
    source: 'El Referente',
    
    image: './assets/images/common/iStock-576902490.jpg',
    url: 'http://www.elreferente.es/sociales/los-medios-sociales-ecologicos-y-agroalimentarios-de-referencia-en-espana-29146',
    video: '',
    videoHash: ''
  },
  { 
    type: 'articulo',
    name: 'Artículo',
    title: 'Talento femenino en el mundo de las tecnologías',
    author: 'Innovaticias.com',
    source: 'Innovaticias.com',
    
    image: './assets/images/common/iStock-503625468.jpg',
    url: 'http://www.innovaticias.com/innovacion/35320/women-in-technology-talento-femenino-sector-ti',
    video: '',
    videoHash: ''
  },
  { 
    type: 'charla',
    name: 'Charla',
    title: 'Sara Werner',
    author: '',
    source: '',
    
    image: './assets/images/common/sara-werner.png',
    url: '',
    video: 'https://www.youtube.com/watch?v=-4a0xD3kv-A',
    videoHash: '-4a0xD3kv-A'
  },

  { 
    type: 'charla',
    name: 'Charla',
    title: 'Alice Fauveau',
    author: '',
    source: '',
    
    image: './assets/images/common/alice-fauveau.png',
    url: '',
    video: 'https://www.youtube.com/watch?v=it2ZKgkYwqM',
    videoHash: 'it2ZKgkYwqM'
  },

  { 
    type: 'charla',
    name: 'Charla',
    title: 'Jordi Martí',
    author: '',
    source: '',
    
    image: './assets/images/common/jordi-marti.png',
    url: '',
    video: 'https://www.youtube.com/watch?v=fS00BVKdJLg',
    videoHash: 'fS00BVKdJLg'
  },

  { 
    type: 'charla',
    name: 'Charla',
    title: 'Emilio Sepúlveda',
    author: '',
    source: '',
    
    image: './assets/images/common/emilio-sepulveda.png',
    url: '',
    video: 'https://www.youtube.com/watch?v=haNgp76PC08',
    videoHash: 'haNgp76PC08'
  },   	    
  { 
     type: 'video',
     name: 'Vídeo',
     title: 'Adriana Freitas',
     author: '',
     source: '',
    
     image: './assets/images/common/adriana-freitas.png',
     url: '',
     video: '73WtF--9hyc',
     videoHash: '73WtF—9hyc'
  },	
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Alicia Palomares',
    author: '',
    source: '',
    
    image: './assets/images/common/alicia-palomares.png',
    url: '',
    video: 'https://youtu.be/G7U8xh-F0mk',
    videoHash: 'G7U8xh-F0mk'
  },
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Anabel Hernández',
    author: '',
    source: '',
    
    image: './assets/images/common/anabel-hernandez.png',
    url: '',
    video: 'https://youtu.be/iQGsXRfYa2k',
    videoHash: 'iQGsXRfYa2k'
  },       
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Gina Tost',
    author: '',
    source: '',
    
    image: './assets/images/common/gina-tost.png',
    url: '',
    video: 'https://youtu.be/-Z-UncgaXc8',
    videoHash: '-Z-UncgaXc8'
  },	    
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Maria Almenar',
    author: '',
    source: '',
    
    image: './assets/images/common/maria-almenar.png',
    url: '',
    video: 'https://youtu.be/rgdX-dMD6pQ',
    videoHash: 'rgdX-dMD6pQ'
  },
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Mari Carmen Martín',
    author: '',
    source: '',
    
    image: './assets/images/common/mari-carmen-martin.png',
    url: '',
    video: 'https://youtu.be/u7xZxx0JYUM',
    videoHash: 'u7xZxx0JYUM'
  },    
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Marta Alonso',
    author: '',
    source: '',
    
    image: './assets/images/common/marta-alonso.png',
    url: '',
    video: 'https://youtu.be/E7eP1ZW4nOc',
    videoHash: 'E7eP1ZW4nOc'
  },    
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Paula Fernández',
    author: '',
    source: '',
    
    image: './assets/images/common/paula-fernandez.png',
    url: '',
    video: 'https://youtu.be/9tz3GiA-z4I',
    videoHash: '9tz3GiA-z4I'
  },    
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Selva Orejón',
    author: '',
    source: '',
    
    image: './assets/images/common/selva-orejon.png',
    url: '',
    video: 'https://youtu.be/eW0yIaC41lE',
    videoHash: 'eW0yIaC41lE'
  },	    
  { 
    type: 'video',
    name: 'Vídeo',
    title: 'Xantal Llavina',
    author: '',
    source: '',
    
    image: './assets/images/common/xantal-llavina.png',
    url: '',
    video: 'https://youtu.be/klpEBwmKpIo',
    videoHash: 'klpEBwmKpIo'
  },
];

@Component({
  moduleId: module.id,
  selector: 'app-inspire',
  templateUrl: 'inspire.component.html',
  styleUrls: ['inspire.component.scss']
})

export class InspireComponent {
  items: InspireItem[];
  navs: Navs[];
  type: string;
  limit: number;
  filter: string;

  constructor() {
    this.items = items;
    this.type = null;
    this.limit = limit;
    this.filter = "todos";    

    // navigation
    this.navs = [
      {
        id: 1,
        name: "todos",
        type: "",
        class: "active"
      },
      {
        id: 2,
        name: "artículos",
        type: "articulo",
        class: ""
      },
      {
        id: 3,
        name: "charlas",
        type: "charla",
        class: ""
      },
      {
        id: 4,
        name: "vídeos",
        type: "video",
        class: ""
      }
    ];    
  }

  ngAfterViewInit () {    
    jQuery('.inspire-item').hide();

    // mostrar todos items      
    jQuery('.inspire-item').each(function (i){       
      if (i <= 11){
        jQuery(this).show();
      }        
    });

    // mostrar boton más      
    jQuery("link-see-more").show(); 
  }

  visible(index: number) {        
    if (index >= this.limit){
      return {'display': 'none'};
    }
  }

  seeMore(): void  {             
     jQuery("link-see-more").hide();
     jQuery('.inspire-item').show();
  }

  setTypeInspirate(type: string): void {   
    this.filter = type;     

    // active link
    jQuery('.navigation-link a').removeClass("active");
    jQuery('.navigation-link__' + this.filter + ' a').addClass("active");    
    // 
    
    if (this.filter !== 'todos' && this.filter !== ''){      
      // filtrar      
      jQuery('.inspire-item').hide();
      jQuery('.inspire-item__' + this.filter).show();     

      // ocultar boton más      
      jQuery("link-see-more").hide();
    }else{
      jQuery('.inspire-item').hide();

      // mostrar todos items      
      jQuery('.inspire-item').each(function (i){        
        if (i <= 11){
          jQuery(this).show();
        }        
      });

      // mostrar boton más      
      jQuery("link-see-more").show();
    }    
  }
}
